<?php

class OurClient_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        
        //Create connect to mongo_db
        $this->mdb->connect();
    }

    function get_all_ourclient()
    {
        $result = $this->mdb->select(['name', 'comment', 'resource', 'is_active'],[])->where_ne('_id','description')->get('our_clients');
        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_all_ourclient_active()
    {
        $result = $this->mdb->select(['name', 'comment', 'resource', 'is_active'],[])->where_ne('_id','description')->where('is_active',1)->get('our_clients');
        return ($result) ? array_to_object($result) : FALSE;
    }



    function get_ourclient_by_id($id)
    {
        $objecId = $this->mdb->create_document_id($id);
        
        $result = $this->mdb->where(['_id' => $objecId])->getOne('our_clients');
        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }



    function get_description_by_id($id)
    {   
        $result = $this->mdb->where(['_id' => $id])->select(['title', 'description', 'is_active'],[])->getOne('our_clients');
        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }



    function create_ourclient($data)
    {
        $data['_id'] = $this->mdb->create_document_id();
        $new_id = $this->mdb->insert('our_clients',$data);
        return $new_id;
    }



    function create_description($data)
    {
        $data['_id'] = 'description';
        $new_id = $this->mdb->insert('our_clients',$data);
        return $new_id;
    }



    function update_description($id, $data)
    {   
        foreach ($data as $key => $value) {
            $this->mdb->set($key, $value);
        }

        $result = $this->mdb->where('_id', $id)->update('our_clients');

        return $result;
    }



    function update_ourclient($id, $data)
    {
        $objecId = $this->mdb->create_document_id($id);
        
        foreach ($data as $key => $value) {
            $this->mdb->set($key, $value);
        }

        $result = $this->mdb->where('_id', $objecId)->update('our_clients');

        return $result;
    }



    function delete_ourclient($id)
    {
        $objecId = $this->mdb->create_document_id($id);

        $result = $this->mdb->where(['_id' => $objecId])->delete('our_clients');

        return $result;
    }
}
