<?php

class About_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        
        //Create connect to mongo_db
        $this->mdb->connect();
    }



    function get()
    {
        $result = $this->mdb->getOne('about');

        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }



    function create($data)
    {
        $data['_id'] = $this->mdb->create_document_id();
        $new_id = $this->mdb->insert('about',$data);
        return $new_id;
    }



    function update($id, $data)
    {
        $objecId = $this->mdb->create_document_id($id);
        
        foreach ($data as $key => $value) {
            $this->mdb->set($key, $value);
        }

        $result = $this->mdb->where('_id', $objecId)->update('about');

        return $result;
    }
}
