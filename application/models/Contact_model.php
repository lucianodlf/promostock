<?php

class Contact_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        
        //Create connect to mongo_db
        $this->mdb->connect();
    }



    function get()
    {
        $result = $this->mdb->getOne('contact');
        
        // $default = (object) [
        //     "title" => 'Dejenos un mensaje',
        //     "phone" => 'PBX (593-2) 2903336 - Telefax: 3237720/8002',
        //     "email" => 'info@promostock.com.ec',
        //     "address" => 'Av. Orellana.E12-126 y 12 de Octubre, Quito, Ecuador',
        //     "resource" => './assets/images/slider-example/slide-1.jpg',
        //     "location" => [
        //         'latitude' => -0.1862504,
        //         'longitude' => -78.5706263,
        //     ]
        // ];

        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }



    function create($data)
    {
        $data['_id'] = $this->mdb->create_document_id();
        $new_id = $this->mdb->insert('contact',$data);
        return $new_id;
    }


    function add_new_contact($data){
        $data['_id'] = $this->mdb->create_document_id();
        $new_id = $this->mdb->insert('users_contact',$data);
        return $new_id;
    }



    function update($id, $data)
    {
        $objecId = $this->mdb->create_document_id($id);
        
        foreach ($data as $key => $value) {
            $this->mdb->set($key, $value);
        }

        $result = $this->mdb->where('_id', $objecId)->update('contact');

        return $result;
    }
}
