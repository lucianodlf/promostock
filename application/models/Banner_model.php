<?php

class Banner_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        
        //Create connect to mongo_db
        $this->mdb->connect();
    }



    function get_all()
    {
        $result = $this->mdb->sort('order', 'asc')->get('banners');
        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_all_active()
    {
        $result = $this->mdb->where(['is_active' => 1])->sort('order', 'asc')->get('banners');
        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_last_order()
    {
        $result = $this->mdb->select(['order'],['_id'])->sort('order','desc')->getOne('banners');
        return ($result) ? $this->mdb->row_array($result)['order'] + 1 : 1;
    }


    function get_by_id($id)
    {
        $objecId = $this->mdb->create_document_id($id);
        
        $result = $this->mdb->where(['_id' => $objecId])->getOne('banners');
        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }



    function create($data)
    {
        $data['_id'] = $this->mdb->create_document_id();
        $new_id = $this->mdb->insert('banners',$data);
        return $new_id;
    }



    function update($id, $data)
    {
        $objecId = $this->mdb->create_document_id($id);
        
        foreach ($data as $key => $value) {
            $this->mdb->set($key, $value);
        }

        $result = $this->mdb->where('_id', $objecId)->update('banners');

        return $result;
    }



    function delete($id)
    {
        $objecId = $this->mdb->create_document_id($id);

        $result = $this->mdb->where(['_id' => $objecId])->delete('banners');

        return $result;
    }
}
