<?php

class Product_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

        //Create connect to mongo_db
        $this->mdb->connect();
    }



    function get_all()
    {
        $result = $this->mdb->sort('name', 'asc')->get('products');
        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_all_reviews_by_product_id($product_id)
    {
        $result = $this->mdb->where(['product' => (string) $product_id])->sort('created_at', 'desc')->get('products_review');
        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_all_active()
    {
        $result = $this->mdb->where(['is_active' => 1])->sort('name', 'asc')->get('products');
        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_news(int $max = 0)
    {
        if ($max === 0) {
            $result = $this->mdb->where(['is_active' => 1])->sort('created_at', 'desc')->get('products');
        } else {
            $result = $this->mdb->where(['is_active' => 1])->limit($max)->sort('created_at', 'desc')->get('products');
        }

        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_deals(int $max = 0)
    {
        if ($max === 0) {
            $result = $this->mdb->where(['is_active' => 1, 'is_deal' => 1])->sort('updated_at', 'desc')->get('products');
        } else {
            $result = $this->mdb->where(['is_active' => 1, 'is_deal' => 1])->limit($max)->sort('updated_at', 'desc')->get('products');
        }

        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_deal_banner(int $max = 0)
    {
        if ($max === 0) {

            $result = $this->mdb->where(['is_active' => 1, 'show_deal_banner' => 1])->sort('updated_at', 'desc')->get('products');
        } else {
            $result = $this->mdb->where(['is_active' => 1, 'show_deal_banner' => 1])->limit($max)->sort('updated_at', 'desc')->get('products');
        }

        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_most_populars(int $max = 0)
    {
        if ($max === 0) {
            $result = $this->mdb->where(['is_active' => 1])->sort('starts_average', 'desc')->get('products');
        } else {
            $result = $this->mdb->where(['is_active' => 1])->limit($max)->sort('starts_average', 'desc')->get('products');
        }

        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_most_views(int $max = 0)
    {
        if ($max === 0) {
            $result = $this->mdb->where(['is_active' => 1])->sort('views', 'desc')->get('products');
        } else {
            $result = $this->mdb->where(['is_active' => 1])->limit($max)->sort('views', 'desc')->get('products');
        }

        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_related(int $max = 0, $category_id, $current_code = 0)
    {
        if (!$category_id) {
            return FALSE;
        }

        if ($max === 0) {
            $result = $this->mdb->where(['is_active' => 1, 'category' => $category_id])->where_ne(['code' => $current_code])->sort('views', 'desc')->get('products');
        } else {
            $result = $this->mdb->where(['is_active' => 1, 'category' => $category_id])->where_ne(['code' => $current_code])->limit($max)->sort('views', 'desc')->get('products');
        }

        return ($result) ? array_to_object($result) : FALSE;
    }


    function get_next_code()
    {
        $result = $this->mdb->select(['code'], ['_id'])->sort('code', 'desc')->getOne('products');
        return ($result) ? $this->mdb->row_array($result)['code'] + 1 : 1;
    }


    function get_starts_by_id($id)
    {
        $objecId = $this->mdb->create_document_id($id);

        $result = $this->mdb->select(['sum_starts', 'total_score'], [])->where(['_id' => $objecId])->getOne('products');
        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }


    function get_by_id($id)
    {
        try {
            $objecId = $this->mdb->create_document_id($id);

            $result = $this->mdb->where(['_id' => $objecId])->getOne('products');
            return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
        } catch (Exception $e) {
            return FALSE;
        }
    }


    function get_resources_by_code($code)
    {
        $result = $this->mdb->select(['code', 'resource_main', 'resource_aditional'], [])->where(['code' => $code])->getOne('products');
        return ($result) ? $this->mdb->row_array($result) : FALSE;
    }


    function get_code_by_code($code)
    {
        $result = $this->mdb->select(['code'], [])->where(['code' => $code])->getOne('products');
        return ($result) ? array_to_object($this->mdb->row_array($result)) : FALSE;
    }


    function create($data)
    {
        // $data['category'] = $this->mdb->create_document_id($data['category']);
        $data['_id'] = $this->mdb->create_document_id();
        $new_id = $this->mdb->insert('products', $data);
        return $new_id;
    }


    function create_review($data)
    {
        $data['_id'] = $this->mdb->create_document_id();
        $new_id = $this->mdb->insert('products_review', $data);
        return $new_id;
    }


    function update_by_code($code, $data)
    {

        foreach ($data as $key => $value) {
            $this->mdb->set($key, $value);
        }

        $result = $this->mdb->where('code', $code)->update('products');
        return $result;
    }


    function update($id, $data)
    {
        $objecId = $this->mdb->create_document_id($id);

        foreach ($data as $key => $value) {
            $this->mdb->set($key, $value);
        }

        $result = $this->mdb->where('_id', $objecId)->update('products');

        return $result;
    }



    function delete($id)
    {
        $objecId = $this->mdb->create_document_id($id);

        $result = $this->mdb->where(['_id' => $objecId])->delete('products');

        return $result;
    }
}
