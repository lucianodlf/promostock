<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('about_config_lang'); ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('edit_form_generic_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('edit_form_generic_lang'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?php if ($about_object->_id != null) {
                            $action = 'about/update';
                        } else {
                            $action = 'about/add';
                        } ?>

                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart($action); ?>
                        <input type="hidden" name="about-id" value="<?= $about_object->_id; ?>">
                        <input type="hidden" name="old-file" value="<?= $about_object->resource; ?>">


                        <div class="row">
                            <div class="col-lg-6">
                                <label><?= translate("about_label_text_first_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <textarea name="text-first" rows="5" class="form-control" placeholder="<?= translate('about_label_text_first_lang'); ?>"><?= $about_object->text_first; ?></textarea>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label><?= translate("about_label_text_second_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <textarea name="text-second" rows="5" class="form-control" placeholder="<?= translate('about_label_text_second_lang'); ?>"><?= $about_object->text_second; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-6">
                                <label><?= translate("about_label_text_mision_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <textarea name="text-mision" rows="5" class="form-control" placeholder="<?= translate('about_label_text_mision_lang'); ?>"><?= $about_object->text_mision; ?></textarea>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label><?= translate("about_label_text_vision_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <textarea name="text-vision" rows="5" class="form-control" placeholder="<?= translate('about_label_text_vision_lang'); ?>"><?= $about_object->text_vision; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-4">
                                <label><?= translate("about_label_title_count_1_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" maxlength="255" class="form-control" name="title-count-1" placeholder="" value="<?= $about_object->title_count_1; ?>">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label><?= translate("about_label_title_count_2_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" maxlength="255" class="form-control" name="title-count-2" placeholder="" value="<?= $about_object->title_count_2; ?>">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label><?= translate("about_label_title_count_3_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" maxlength="255" class="form-control" name="title-count-3" placeholder="" value="<?= $about_object->title_count_3; ?>">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-4">
                                <label><?= translate("about_label_number_count_1_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-sort-numeric-asc"></i></span>
                                    <input type="number" class="form-control" name="number-count-1" placeholder="1" value="<?= $about_object->number_count_1; ?>">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label><?= translate("about_label_number_count_2_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-sort-numeric-asc"></i></span>
                                    <input type="number" class="form-control" name="number-count-2" placeholder="2" value="<?= $about_object->number_count_2; ?>">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label><?= translate("about_label_number_count_3_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-sort-numeric-asc"></i></span>
                                    <input type="number" class="form-control" name="number-count-3" placeholder="3" value="<?= $about_object->number_count_3; ?>">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-6">
                                <label><?= translate("about_label_img_subh_lang"); ?> (1873 x 350)</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                    <input id="image-upload" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" name="archivo" <?= ($about_object->resource == null) ? 'required' : ''; ?>>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label><?= translate("about_label_img_first_lang"); ?> (1000 x 1000)</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                    <input id="image-upload-2" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" name="archivo-2" <?= ($about_object->resource_2 == null) ? 'required' : ''; ?>>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <?php if (strlen($about_object->resource) > 0) { ?>

                                    <img style="padding: 1px; border: 1px solid #F98A3C; width: 80%; margin-top: 25px;" class="img img-rounded img-responsive" src="<?= base_url($about_object->resource); ?>" />
                                    <label>Imagen SubHeader Actual</label>

                                <?php } else { ?>

                                    <label>Sin imagen cargada</label>

                                <?php } ?>
                            </div>
                            <div class="col-lg-6">
                                <?php if (strlen($about_object->resource_2) > 0) { ?>

                                    <img style="padding: 1px; border: 1px solid #F98A3C; width: 50%; margin-top: 25px;" class="img img-rounded img-responsive" src="<?= base_url($about_object->resource_2); ?>" />
                                    <label>Imagen Primaria Actual</label>

                                <?php } else { ?>

                                    <label>Sin imagen cargada</label>

                                <?php } ?>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12" style="text-align: left;">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                            </div>
                        </div>

                        <?= form_close(); ?>

                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(function() {

    });
</script>