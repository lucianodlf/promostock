<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Editar credencial | <?= $this->session->userdata('name') ?> | <a href="<?= site_url('user/index'); ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> <?= translate('back_lang'); ?> </a>

        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('edit_credential_lang'); ?></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?= translate('edit_credential_lang'); ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart("user/edit_credential"); ?>

                        <input type="hidden" name="user_id" value="<?= $this->session->userdata('user_id'); ?>">
                        <input type="hidden" name="email_old" value="<?= $this->session->userdata('email'); ?>">
                        <input type="hidden" name="validacion" value=0>

                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label><?= translate("email_label_lang"); ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon">@</span>
                                        <input type="email" class="form-control input-sm" required name="email" value="<?= (isset($user_object)) ? $user_object->email : set_value('email'); ?>" placeholder="<?= translate('email_label_lang') ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label><?= translate("current_pwd_label_lang"); ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input type="password" id="password" name="current_pwd" value="<?= set_value('current_pwd'); ?>" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label><?= translate("new_pwd_label_lang"); ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input type="password" id="password" name="new_pwd" value="" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label><?= translate("repeat_new_pwd_label_lang"); ?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input type="password" id="password" name="new_repeat_pwd" value="" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> <?= translate('save_btn_lang'); ?></button>
                                <a class="btn btn-default" href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-arrow-circle-left"></i> <?= translate('cancel_btn_lang'); ?></a>
                            </div>
                        </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

</div>

<script type="text/javascript">
    $(function() {

    });
</script>