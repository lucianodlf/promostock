<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('user_config_lang'); ?>
            <small><?= translate('new_user_lang'); ?></small>
            | <a href="<?= site_url('user/index'); ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> <?= translate('back_lang'); ?>
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('user_list_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('add_user_lang'); ?></h3>
                    </div>
                    <div class="box-body">

                        <?= get_message_from_operation(); ?>

                        <?= form_open("user/add"); ?>
                        <div class="row">
                            <div class="col-lg-4">
                                <label><?= translate("name_label_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" maxlength="255" class="form-control input-sm" name="fullname" required placeholder="<?= translate('name_ph_lang'); ?>" value="<?= set_value('fullname'); ?>">
                                </div>

                                <br>

                                <label><?= translate("pwd_label_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" class="form-control input-sm" name="password" maxlength="255" required placeholder="<?= translate('pwd_ph_lang'); ?>">
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <label><?= translate("phone_label_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                    <input type="text" maxlength="255" class="form-control input-sm" name="phone" placeholder="<?= translate('phone_ph_lang'); ?>" value="<?= set_value('phone'); ?>">
                                </div>

                                <br>

                                <label><?= translate("repeat_pwd_label_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" class="form-control input-sm" name="repeat_password" required placeholder="<?= translate('pwd_ph_lang'); ?>">
                                </div>

                                <br>

                            </div>
                            <div class="col-lg-4">
                                <label><?= translate("role_label_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <select id="role" name="role" class="form-control select2 input-sm" style="width: 100%">

                                        <?php if (isset($all_roles)) { ?>
                                            <?php foreach ($all_roles as $item) { ?>
                                                <option value="<?= $item->_id; ?>"><?= $item->name; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>

                                <br>

                                <label><?= translate("email_label_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon">@</span>
                                    <input type="email" class="form-control input-sm" required name="email" placeholder="<?= translate('email_ph_lang') ?>" value="<?= set_value('email'); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <br>
                            <div class="col-xs-12" style="text-align: right;">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                            </div>
                        </div>

                        <?= form_close(); ?>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    $(function() {

    });
</script>