<!-- Start Breadcrumbs Section -->
<section class="breadcrumbs-section background_bg" data-img-src="<?= base_url($about_object->resource); ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="page_title text-center">
					<h1>Sobre Nosotros</h1>
					<ul class="breadcrumb justify-content-center">
						<li><a href="<?= site_url(); ?>">Inicio</a></li>
						<li><span>Sobre Nosotros</span></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Header Section -->

<!-- Start Contact Detail Section -->
<section class="aboutus-inner-page pt_large pb_large">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="about-info-img">
					<img src="<?= base_url($about_object->resource_2); ?>" alt="about-img">
				</div>
			</div>
			<div class="col-md-6">
				<div class="about-info text-justify">
					<p><?= $about_object->text_first; ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Contact Detail Section -->


<!-- Start Contact Detail Section -->
<section class="aboutus-inner-page" style="padding: 0;">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 text-center" style="background-image: url(<?= base_url('assets/images/bg-mision.jpg'); ?>); padding: 100px 0;">
				<i style="color: #fff;" class="fa fa-star fa-5x"></i>
				<h1 style="font-weight: 700; color: #fff; margin-top: 10px;">Misión</h1>
				<br>
				<div class="about-info mision text-center">
					<p><?= $about_object->text_mision; ?></p>
				</div>
			</div>
			<div class="col-md-6 text-center" style="background-image: url(<?= base_url('assets/images/bg-vision.jpg'); ?>); padding: 100px 0;">
				<i style="color: #fff;" class="fa fa-eye fa-5x"></i>
				<br>
				<h1 style="font-weight: 700; color:#fff; margin-top: 10px;">Visión</h1>
				<br>
				<div class="about-info vision text-center">
					<p><?= $about_object->text_vision; ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Contact Detail Section -->

<!-- Start Contact Detail Section -->
<section class="about-section-3">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<p class="text-center"><img src="<?= base_url('assets/images/ecuador-map.png'); ?>" alt=""></p>
			</div>
			<div class="col-md-6 col-align">
				<h3 style="font-weight: 700; text-align: center; margin-top: 25%">Promostock Cerca de Ti</h3>
				<hr style="width: 80px;
					border-color: #F98A3C !important;">
				<p style="text-align: justify;">Nuestras oficinas de Quito y Guayaquil nos permiten estar cerca y abastecer de mejor manera al cliente con las soluciones promocionales que necesita, cuando las necesita, donde las necesita.</p>
			</div>
		</div>
	</div>
</section>
<!-- End Contact Detail Section -->

<!-- Start Contact Detail Section -->
<section class="about-section-counter" style="background-image: url(<?= base_url('assets/images/bg-footer.jpg'); ?>)">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="counter-inner">
					<img src="<?= base_url('assets/images/icon-1.png'); ?>" alt="">
					<br>
					<span class="counter"><?= $about_object->number_count_1; ?></span>
					<p><?= $about_object->title_count_1; ?></p>
				</div>
				<div class="counter-inner">
					<img src="<?= base_url('assets/images/icon-2.png'); ?>" alt="">
					<br>
					<span class="counter"><?= $about_object->number_count_2; ?></span>
					<p><?= $about_object->title_count_2; ?></p>
				</div>
				<div class="counter-inner">
					<img src="<?= base_url('/assets/images/icon-3.png'); ?>" alt="">
					<br>
					<span class="counter"><?= $about_object->number_count_3; ?></span>
					<p><?= $about_object->title_count_3; ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Contact Detail Section -->

<!-- Start Event Section-->
<section class="event-section pt_large">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1">
			</div>
			<div class="col-md-2 text-center">
				<img src="<?= base_url('assets/images/001-box.png'); ?>" alt="">
				<br><br>
				<h6>Productos <br>& Servicios</h6>
				<br>
				<ul>
					<li><p>Plásticos</p></li>
					<li><p>Textiles</p></li>
					<li><p>Importaciones</p></li>
					<li><p>Licencias</p></li>
					<li><p>Copacking</p></li>
					<li><p>Logística</p></li>
				</ul>
			</div>
			<div class="col-md-2 text-center">
				<img src="<?= base_url('assets/images/001-innovation.png'); ?>" alt="">
				<br><br>
				<h6>Desarrollo <br>& Innovación</h6>
				<br>
				<ul>
					<li><p>Tendencias Globales</p></li>
					<li><p>Desarrollo de moldes para inyección</p></li>
					<li><p>Marcar Tendencias</p></li>
					<li><p>Ferias Internacionales</p></li>
				</ul>
			</div>
			<div class="col-md-2 text-center">
				<img src="<?= base_url('assets/images/002-network.png'); ?>" alt="">
				<br><br>
				<h6>Inteligencia de <br> Mercado</h6>
				<br>
				<ul>
					<li><p>Data</p></li>
					<li><p>Información</p></li>
					<li><p>Conocimiento</p></li>
				</ul>
			</div>
			<div class="col-md-2 text-center">
				<img src="<?= base_url('assets/images/002-options.png'); ?>" alt="">
				<br><br>
				<h6>Alianzas <br>Estratégicas</h6>
				<br>
				<ul>
					<li><p>Industrias</p></li>
					<li><p>Licencias</p></li>
					<li><p>Traders</p></li>
				</ul>
			</div>
			<div class="col-md-2 text-center">
				<img src="<?= base_url('assets/images/003-customer-service.png'); ?>" alt="">
				<br><br>
				<h6>Atención <br>& Servicio</h6>
				<br>
				<ul>
					<li><p>Asesoría Personalizada</p></li>
					<li><p>Herramientas Informáticas</p></li>
					<li><p>Eventos & Workshops</p></li>
				</ul>
			</div>
			<div class="col-md-1">
			</div>
		</div>
	</div>
</section>
<!-- End Event Section-->

<!-- Start Review Section -->
<!-- <section class="review-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="carousel_slide1 review-slider bg_red same-nav owl-carousel owl-theme owl-loaded owl-drag">
					<div class="item review-box">
						<div class="client-img">
							<img src="image/client-img-1.png" alt="client-img">
						</div>
						<div class="client-text">
							<div class="quote-icon"><img src="image/quote-icon.png" alt="quote-icon"></div>
							<p>Maecenas semper aliquam massa. Praesent pharetra semvitae nisi eleifend molestie.Aliquam molestie scelerisque ultricies. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor dictum ornare.</p>
							<a href="#">- William Genske</a>
						</div>
					</div>
					<div class="item review-box">
						<div class="client-img">
							<img src="image/client-img-1.png" alt="client-img">
						</div>
						<div class="client-text">
							<div class="quote-icon"><img src="image/quote-icon.png" alt="quote-icon"></div>
							<p>Maecenas semper aliquam massa. Praesent pharetra semvitae nisi eleifend molestie.Aliquam molestie scelerisque ultricies. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor dictum ornare.</p>
							<a href="#">- William Genske</a>
						</div>
					</div>
					<div class="item review-box">
						<div class="client-img">
							<img src="image/client-img-1.png" alt="client-img">
						</div>
						<div class="client-text">
							<div class="quote-icon"><img src="image/quote-icon.png" alt="quote-icon"></div>
							<p>Maecenas semper aliquam massa. Praesent pharetra semvitae nisi eleifend molestie.Aliquam molestie scelerisque ultricies. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempor dictum ornare.</p>
							<a href="#">- William Genske</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->
<!-- End Review Section -->

<!-- Start Facility Section-->
<!-- <section class="facility-section pb_large">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-6 facility-box box-1">
				<div class="facility-inner">
					<div class="fb-icon">
						<i class="fa fa-truck"></i>
					</div>
					<div class="fb-text">
						<h5>FREE DELIVERY</h5>
						<span>Worldwide</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-6 facility-box box-2">
				<div class="facility-inner">
					<div class="fb-icon">
						<i class="fa fa-headphones"></i>
					</div>
					<div class="fb-text">
						<h5>24/ 7 SUPPORT</h5>
						<span>Customer Support</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-6 facility-box box-3">
				<div class="facility-inner">
					<div class="fb-icon">
						<i class="fa fa-cc-mastercard"></i>
					</div>
					<div class="fb-text">
						<h5>PAYMENT</h5>
						<span>Secure System</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-6 facility-box box-4">
				<div class="facility-inner">
					<div class="fb-icon">
						<i class="fa fa-trophy"></i>
					</div>
					<div class="fb-text">
						<h5>TRUSTED</h5>
						<span>enuine Products</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->
<!-- End Facility Section-->