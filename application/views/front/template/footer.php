<!-- Start Footer Section -->
<footer>
	<div class="footer-top" style="background-image: url(<?= base_url('assets/images/bg-footer.jpg') ?>)">
		<div class="container">			
			<div class="row">
				<div class="col-lg-4 col-md-12 col-sm-12">
					<div class="footer-box box-1">
						<img src="<?= base_url($this->session->userdata('logos')['footer']->resource);?>">
						<br>
						<h6 class="fb-title"><?= $footer->title_1; ?></h6>
						<div class="fb-iner">
							<p><?= $footer->text; ?></p>
						</div>
					</div> 
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 order-md-last">
					<div class="footer-box box-4">
						<h6 class="fb-title"><?= $footer->title_4; ?></h6>
						<div class="fb-iner">
							<ul>
								<li><i class="ion-ios-location-outline"></i><p><strong>Quito:</strong> <?= $contact->address; ?></p></li>
								<li><i class="ion-ios-location-outline"></i><p><strong>Guayaquil:</strong> <?= $contact->address_2; ?></p></li>
								<li><i class="ion-ios-telephone-outline"></i><p>
								<?php $html_phone = ''; $last = count($contact->phone) - 1;
								foreach($contact->phone as $idx => $phone){
									$html_phone .= '<a href="tel:'.$phone['tel'].'">' . $phone['string'] . '</a>';
									if($idx < $last){
										$html_phone .= ' / ';
									}
								};
								$html_phone .= '</p></li>';
								echo $html_phone;
								?>
								<!-- <li><i class="ion-ios-telephone-outline"></i><p><a href="tel:+59322903336">PBX (593-2) 2903336</a> - Telfax: <a href="tel:+5933237720">3237720</a>/<a href="tel:+5933238002">8002</a></p></li> -->
								<li><i class="ion-ios-email-outline"></i><a href="mailto:<?= $contact->email; ?>"><?= $contact->email; ?></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-3 col-6">
					<div class="footer-box box-2">
						<h6 class="fb-title"><?= $footer->title_2; ?></h6>
						<div class="fb-iner">
							<ul class="footer-links">
								<li><a href="http://www.mercico.com">Mercico</a></li>
								<li><a href="http://www.opnmind.com.co">Open Mind</a></li>
								<li><a href="http://www.topasa.com">Topasa</a></li>
								<li><a href="http://www.lotusglobalmkt.com">Lotus Global MKT</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-3 col-6">
					<div class="footer-box box-3">
						<h6 class="fb-title"><?= $footer->title_3; ?></h6>
						<div class="fb-iner">
							<ul class="footer-links">
								<li><a href="#">Ordenes</a></li>
								<li><a href="#">Wishlist</a></li>
								<li><a href="#">Log In</a></li>
								<li><a href="#">Register</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-btm" style="background-image: url(<?= base_url('assets/images/bg-footer.jpg') ?>)">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="copyright">
						<p>Copyright © <?= date('Y');?>. Todos los derechos reservados.</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="footer-social text-md-right">
						<ul>
							<li><a href="<?= $footer->link_fb; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a href="<?= $footer->link_instagram; ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
							<li><a href="<?= $footer->link_linkedin; ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
							<!-- <li><a href="<?= $footer->link_twitter; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a href="<?= $footer->link_pinteres; ?>" target="_blank"><i class="fa fa-pinterest-p"></i></a></li>
							<li><a href="<?= $footer->link_youtube; ?>" target="_blank"><i class="fa fa-youtube"></i></a></li> -->
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- End Footer Section -->


<!-- Home Popup Section -->
<div class="modal fade bd-example-modal-lg main-popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body p-0">
				<div class="newsletter-pop-up d-flex">
					<div class="popup-img">
						<img src="templates/atz_shop/image/1_popup-img.jpg" alt="popup-img">
					</div>
					<div class="popup-form text-center">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><i class="ion-close-round"></i></span>
						</button>
						<div class="popup-logo">
							<img src="templates/atz_shop/image/logo.png" alt="logo">
						</div>
						<div class="popup-text">
							<p>Join us Now</p>
							<h6>And get hot news about the theme</h6>
						</div>
						<form class="subscribe-popup-form" method="post" action="#">
							<input name="email" required type="email" placeholder="Enter Your Email">
							<button class="btn btn-primary" title="Subscribe" type="submit">Subscribe</button>
						</form>
						<div class="form-check">
							<label>Don't show this popup again!
								<input class="defult-check" type="checkbox" checked="checked">
								<span class="checkmark"></span>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Screen Load Popup Section -->

<!-- Start Quickview Popup Section -->
<div id="popup-detail-product" class="white-popup quickview-popup mfp-hide">
<div id="alert-error-msg" class="alert alert-error" style="display: none"></div>
	<div class="row">
		<div class="col-md-5">
			<div class="product-image">
				<img class="product_img" src='' data-zoom-image=""/>
			</div>
			<div id="product_gallery" class="product_gallery_item owl-thumbs-slider owl-carousel owl-theme">
				<div class="item">
					<a href="#" class="active" data-image="" data-zoom-image="">
						<img src="" />
					</a>
				</div>
				<!-- <div class="item">
					<a href="#" data-image="templates/atz_shop/image/product2.png" data-zoom-image="templates/atz_shop/image/product2.png">
						<img src="templates/atz_shop/image/product_small2.png" />
					</a>
				</div> -->
			</div>
		</div>
		<div class="col-md-7">
			<div class="quickview-product-detail">
				<h2 class="box-title">Ornare sed consequat</h2>
				<h3 class="box-price"><del>$ 95.00</del>$ 81.00</h3>
				<p class="box-text">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
				<p class="stock">Availability: <span>In Stock</span></p>
				<div class="quantity-box">
					<p>Cantidad:</p>
					<div class="input-group">
						<input type="button" value="-" class="minus">
						<input class="quantity-number qty" type="text" value="1" min="1" max="10">
						<input type="button" value="+" class="plus">
					</div>
					<div class="quickview-cart-btn">
						<a href="#" class="btn btn-primary"><img src="<?= base_url(); ?>assets/templates/atz_shop/image/cart-icon-1.png" alt="cart-icon-1"> Agregar al carrito</a>
					</div>
				</div>
				<div class="box-social-like d-sm-flex justify-content-between">
					<ul class="hover-icon box-like">
						<!-- <li><a href="#"><i class="fa fa-heart"></i></a></li>
						<li><a href="#"><i class="fa fa-refresh"></i></a></li> -->
					</ul>
					<ul class="hover-icon box-social">
						<!-- <li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Quickview Popup Section -->

<!-- Jquery js -->
<!-- <script src="<?= base_url(); ?>/assets/templates/atz_shop/js/jquery.min.js" type="text/javascript"></script> -->
<!-- popper.min js -->
<script src="<?= base_url(); ?>/assets/templates/atz_shop/js/popper.min.js" type="text/javascript"></script>
<!-- Bootstrap js -->
<script src="<?= base_url(); ?>/assets/templates/atz_shop/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Magnific Popup js -->
<script src="<?= base_url(); ?>/assets/templates/atz_shop/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
<!-- Map js -->
<!-- Original template -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7TypZFTl4Z3gVtikNOdGSfNTpnmq-ahQ"  type="text/javascript"></script> -->
<!-- New pedro -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0jIY1DdGJ7yWZrPDmhCiupu_K2En_4HY" type="text/javascript"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0jIY1DdGJ7yWZrPDmhCiupu_K2En_4HY&libraries=places" type="text/javascript"></script>

<!-- Owl js -->
<script src="<?= base_url(); ?>/assets/templates/atz_shop/js/owl.carousel.min.js" type="text/javascript"></script>
<!-- Countdown js -->
<script src="<?= base_url(); ?>/assets/templates/atz_shop/js/countdown.min.js" type="text/jscript"></script>
<!-- Counter js -->
<script src="<?= base_url(); ?>/assets/templates/atz_shop/js/jquery.countup.js" type="text/javascript"></script>
<!-- waypoint js -->
<script src="<?= base_url(); ?>/assets/templates/atz_shop/js/waypoint.js" type="text/javascript"></script>
<!-- Select2 js -->
<script src="<?= base_url(); ?>/assets/templates/atz_shop/js/select2.min.js" type="text/javascript"></script>
<!-- Price Slider js -->
<script src="<?= base_url(); ?>/assets/templates/atz_shop/js/jquery-price-slider.js" type="text/javascript"></script>
<!-- jquery.elevatezoom js -->
<script src='<?= base_url(); ?>/assets/templates/atz_shop/js/jquery.elevatezoom.js' type="text/javascript"></script>
<!-- imagesloaded.pkgd.min js -->
<script src='<?= base_url(); ?>/assets/templates/atz_shop/js/imagesloaded.pkgd.min.js' type="text/javascript"></script>
<!-- isotope.min js -->
<script src='<?= base_url(); ?>/assets/templates/atz_shop/js/isotope.min.js' type="text/javascript"></script>
<!-- jquery.fitvids js -->
<script src='<?= base_url(); ?>/assets/templates/atz_shop/js/jquery.fitvids.js' type="text/javascript"></script>
<!-- mCustomScrollbar.concat.min js -->
<script src="<?= base_url(); ?>/assets/templates/atz_shop/js/mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<!-- Custom css -->
<script src="<?= base_url(); ?>/assets/templates/atz_shop/js/custom.js" type="text/javascript"></script> 

<!-- Custom promostock -->
<script src="<?= base_url(); ?>/assets/js/promostock.js" type="text/javascript"></script> 

</body>
</html>	