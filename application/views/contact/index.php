<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('contact_config_lang'); ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('edit_form_generic_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('edit_form_generic_lang'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?php if ($contact_object->_id != null) {
                            $action = 'contact/update';
                        } else {
                            $action = 'contact/add';
                        } ?>

                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart($action); ?>
                        <input type="hidden" name="contact-id" value="<?= $contact_object->_id; ?>">
                        <input type="hidden" name="old-file" value="<?= $contact_object->resource; ?>">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label><?= translate("form_label_title_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="title" placeholder="<?= translate('form_ph_title_lang'); ?>" value="<?= $contact_object->title; ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label><?= translate("form_label_title_2_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="title-2" placeholder="<?= translate('form_ph_title_lang'); ?>" value="<?= $contact_object->title_2; ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label><?= translate("form_label_address_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="address" placeholder="<?= translate('form_ph_address_lang'); ?>" value="<?= $contact_object->address; ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label><?= translate("form_label_address_2_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="address-2" placeholder="<?= translate('form_ph_address_lang'); ?>" value="<?= $contact_object->address_2; ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label><?= translate("form_label_phone_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="phone" placeholder="<?= translate('form_ph_phone_lang'); ?>" value="<?= $contact_object->phone; ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label><?= translate("form_label_email_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="email" maxlength="255" class="form-control" name="email" placeholder="<?= translate('form_ph_email_lang'); ?>" value="<?= $contact_object->email; ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label><?= translate("contact_label_latitude_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="number" step="any" maxlength="255" class="form-control" name="latitude" placeholder="-31.640211" value="<?= $contact_object->location->latitude; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label><?= translate("contact_label_longuitude_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="number" step="any" maxlength="255" class="form-control" name="longuitude" placeholder="-60.680754" value="<?= $contact_object->location->longuitude; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label><?= translate("contact_label_latitude_2_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="number" step="any" maxlength="255" class="form-control" name="latitude-2" placeholder="-60.680754" value="<?= $contact_object->location->latitude_2; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label><?= translate("contact_label_longuitude_2_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="number" step="any" maxlength="255" class="form-control" name="longuitude-2" placeholder="-60.680754" value="<?= $contact_object->location->longuitude_2; ?>">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label><?= translate("contact_label_text_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <textarea name="text" rows="5" class="form-control" placeholder="<?= translate('contact_label_text_lang'); ?>"><?= $contact_object->text; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-6">
                                            <label><?= translate("form_label_image_lang"); ?> (1873 X 350)</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                                <input id="image_upload" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" name="archivo" <?= ($contact_object->resource == null) ? 'required' : ''; ?>>
                                            </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <?php if (strlen($contact_object->resource) > 0) { ?>
                                            <label>Imagen SubHeader Actual</label>
                                            <img style="padding: 1px; width: 100%; border: 1px solid #F98A3C; margin-top: 1px;" class="img img-rounded img-responsive" src="<?= base_url($contact_object->resource); ?>" />

                                        <?php } else { ?>

                                            <label>Sin imagen cargada</label>

                                        <?php } ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: left;">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                                    </div>
                                </div>
                            </div>
                            <?= form_close(); ?>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(function() {

    });
</script>