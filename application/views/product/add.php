<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('product_config_lang'); ?>
            <small><?= translate('new_product_lang'); ?></small>
            | <a href="<?= site_url('product/index'); ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> <?= translate('back_lang'); ?>
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('new_product_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('new_product_lang'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart("product/add"); ?>

                        <div class="row">
                            <div class="col-lg-4">
                                <label><?= translate("product_label_code_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="number" class="form-control" name="code" placeholder="<?= translate('product_label_code_lang'); ?>" value="<?= ($next_code) ? $next_code : 0; ?>">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label><?= translate("product_label_name_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" maxlength="255" class="form-control" name="name" required placeholder="<?= translate('product_label_name_lang'); ?>">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label><?= translate("product_label_category_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-align-justify"></i></span>
                                    <select name="category" class="form-control">
                                        <?php foreach ($categorys_product as $category) { ?>
                                            <option value="<?= $category->_id; ?>"><?= $category->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-3">
                                <label><?= translate("product_label_starts_init_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-align-justify"></i></span>
                                    <select name="starts" class="form-control">
                                        <option value="0" selected><?= translate('product_option_starts_default_lang'); ?></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <label><?= translate("product_label_stock_quantity_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="number" class="form-control" name="stock_quantity" value="<?= ($default_stock_quantity) ? $default_stock_quantity : 0; ?>">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <label><?= translate("product_label_is_deal_lang"); ?></label>
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="is_deal" value="0" checked="">
                                            <?= translate('text_option_not'); ?>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="is_deal" value="1">
                                            <?= translate('text_option_yes'); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <label><?= translate("product_label_is_show_deal_banner_lang"); ?></label>
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="is_show_deal_banner" value="0" checked="">
                                            <?= translate('text_option_not'); ?>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="is_show_deal_banner" value="1">
                                            <?= translate('text_option_yes'); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-6">
                                <label><?= translate("product_label_description_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <textarea name="description" rows="4" class="form-control" required placeholder="<?= translate('product_label_description_lang'); ?>"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label><?= translate("product_label_price_f_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="number" step="any" class="form-control" name="price-final" placeholder="100.00">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-6">
                                <label><?= translate("product_label_main_image_lang"); ?> (<?= "$img_width X $img_height" ?>)</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                    <input id="image-upload" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" required name="archivo">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label>Sin imagen cargada</label>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12" style="text-align: left;">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                            </div>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
</div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(function() {

    });
</script>