<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('product_config_lang'); ?>
            <small><?= translate('edit_product_lang'); ?></small>
            | <a href="<?= site_url('product/update_index/' . $product_object->_id); ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> <?= translate('back_lang'); ?>
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('edit_product_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('edit_product_lang'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart("product/update_aditional_images"); ?>

                        <input type="hidden" name="product-id" value="<?= $product_object->_id; ?>">
                        <input type="hidden" name="code" value="<?= $product_object->code; ?>">
                        <input type="hidden" name="old_aditional_image_1" value="<?= $product_object->aditional_information->images->resource_aditional_1->path; ?>">
                        <input type="hidden" name="old_aditional_image_2" value="<?= $product_object->aditional_information->images->resource_aditional_2->path; ?>">
                        <input type="hidden" name="old_aditional_image_3" value="<?= $product_object->aditional_information->images->resource_aditional_3->path; ?>">
                        <input type="hidden" name="old_aditional_image_4" value="<?= $product_object->aditional_information->images->resource_aditional_4->path; ?>">

                        <!----------------------- IMAGEN ADITIONAL 1 ---------------------------->
                        <div class="row">
                            <div class="col-lg-2">
                                <label><?= translate("product_label_is_show_deal_banner_lang"); ?></label>
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="show_deal_banner_resource_aditional_1" value="0" <?= (!$product_object->aditional_information->images->resource_aditional_1->show_deal_banner) ? 'checked=""' : ""; ?>>
                                            <?= translate('text_option_not'); ?>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="show_deal_banner_resource_aditional_1" value="1" <?= ($product_object->aditional_information->images->resource_aditional_1->show_deal_banner) ? 'checked=""' : ""; ?>>
                                            <?= translate('text_option_yes'); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <label><?= translate("product_label_aditional_image_lang") . " (1)"; ?> (<?= "$img_width X $img_height" ?>)</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                    <input id="image-upload-1" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" name="resource_aditional_1">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <?php if (strlen($product_object->aditional_information->images->resource_aditional_1->path) > 0) { ?>

                                    <label>Imagen Actual</label>
                                    <img style="padding: 1px; width: 50%; border: 1px solid #F98A3C; margin-top: 1px;" class="img img-rounded img-responsive" src="<?= base_url($product_object->aditional_information->images->resource_aditional_1->small); ?>" />
                                <?php } else { ?>

                                    <label>Sin imagen cargada</label>
                                <?php } ?>
                            </div>
                            <div class="col-lg-3">
                                <?php if (strlen($product_object->aditional_information->images->resource_aditional_1->path) > 0) { ?>
                                    <div class="col-xs-12" style="text-align: center; margin: 15% 0;">
                                        <button type="submit" onclick="deleteAditionalImage('<?= $product_object->_id; ?>','<?= $product_object->aditional_information->images->resource_aditional_1->path; ?>','resource_aditional_1')" class="btn btn-danger btn-delete"><i class="fa fa-trash-o"></i> <?= translate('delete_btn_lang'); ?></button>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                        <hr>

                        <!----------------------- IMAGEN ADITIONAL 2 ---------------------------->
                        <div class="row">
                            <div class="col-lg-2">
                                <label><?= translate("product_label_is_show_deal_banner_lang"); ?></label>
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="show_deal_banner_resource_aditional_2" value="0" <?= (!$product_object->aditional_information->images->resource_aditional_2->show_deal_banner) ? 'checked=""' : ""; ?>>
                                            <?= translate('text_option_not'); ?>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="show_deal_banner_resource_aditional_2" value="1" <?= ($product_object->aditional_information->images->resource_aditional_2->show_deal_banner) ? 'checked=""' : ""; ?>>
                                            <?= translate('text_option_yes'); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <label><?= translate("product_label_aditional_image_lang") . " (2)"; ?> (<?= "$img_width X $img_height" ?>)</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                    <input id="image-upload-2" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" name="resource_aditional_2">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <?php if (strlen($product_object->aditional_information->images->resource_aditional_2->path) > 0) { ?>

                                    <label>Imagen Actual</label>
                                    <img style="padding: 1px; width: 50%; border: 1px solid #F98A3C; margin-top: 1px;" class="img img-rounded img-responsive" src="<?= base_url($product_object->aditional_information->images->resource_aditional_2->small); ?>" />
                                <?php } else { ?>

                                    <label>Sin imagen cargada</label>
                                <?php } ?>
                            </div>
                            <div class="col-lg-3">
                                <?php if (strlen($product_object->aditional_information->images->resource_aditional_2->path) > 0) { ?>
                                    <div class="col-xs-12" style="text-align: center; margin: 15% 0;">
                                        <button type="submit" onclick="deleteAditionalImage('<?= $product_object->_id; ?>','<?= $product_object->aditional_information->images->resource_aditional_2->path; ?>','resource_aditional_2')" class="btn btn-danger btn-delete"><i class="fa fa-trash-o"></i> <?= translate('delete_btn_lang'); ?></button>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                        <hr>

                        <!----------------------- IMAGEN ADITIONAL 3 ---------------------------->
                        <div class="row">
                            <div class="col-lg-2">
                                <label><?= translate("product_label_is_show_deal_banner_lang"); ?></label>
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="show_deal_banner_resource_aditional_3" value="0" <?= (!$product_object->aditional_information->images->resource_aditional_3->show_deal_banner) ? 'checked=""' : ""; ?>>
                                            <?= translate('text_option_not'); ?>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="show_deal_banner_resource_aditional_3" value="1" <?= ($product_object->aditional_information->images->resource_aditional_3->show_deal_banner) ? 'checked=""' : ""; ?>>
                                            <?= translate('text_option_yes'); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <label><?= translate("product_label_aditional_image_lang") . " (3)"; ?> (<?= "$img_width X $img_height" ?>)</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                    <input id="image-upload-3" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" name="resource_aditional_3">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <?php if (strlen($product_object->aditional_information->images->resource_aditional_3->path) > 0) { ?>

                                    <label>Imagen Actual</label>
                                    <img style="padding: 1px; width: 50%; border: 1px solid #F98A3C; margin-top: 1px;" class="img img-rounded img-responsive" src="<?= base_url($product_object->aditional_information->images->resource_aditional_3->small); ?>" />
                                <?php } else { ?>

                                    <label>Sin imagen cargada</label>
                                <?php } ?>
                            </div>
                            <div class="col-lg-3">
                                <?php if (strlen($product_object->aditional_information->images->resource_aditional_3->path) > 0) { ?>
                                    <div class="col-xs-12" style="text-align: center; margin: 15% 0;">
                                        <button type="submit" onclick="deleteAditionalImage('<?= $product_object->_id; ?>','<?= $product_object->aditional_information->images->resource_aditional_3->path; ?>','resource_aditional_3')" class="btn btn-danger btn-delete"><i class="fa fa-trash-o"></i> <?= translate('delete_btn_lang'); ?></button>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                        <hr>

                        <!----------------------- IMAGEN ADITIONAL 4 ---------------------------->
                        <div class="row">
                            <div class="col-lg-2">
                                <label><?= translate("product_label_is_show_deal_banner_lang"); ?></label>
                                <div class="form-group">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="show_deal_banner_resource_aditional_4" value="0" <?= (!$product_object->aditional_information->images->resource_aditional_4->show_deal_banner) ? 'checked=""' : ""; ?>>
                                            <?= translate('text_option_not'); ?>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="show_deal_banner_resource_aditional_4" value="1" <?= ($product_object->aditional_information->images->resource_aditional_4->show_deal_banner) ? 'checked=""' : ""; ?>>
                                            <?= translate('text_option_yes'); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <label><?= translate("product_label_aditional_image_lang") . " (4)"; ?> (<?= "$img_width X $img_height" ?>)</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-image"></i></span>
                                    <input id="image-upload-4" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" class="form-control" name="resource_aditional_4">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <?php if (strlen($product_object->aditional_information->images->resource_aditional_4->path) > 0) { ?>

                                    <label>Imagen Actual</label>
                                    <img style="padding: 1px; width: 50%; border: 1px solid #F98A3C; margin-top: 1px;" class="img img-rounded img-responsive" src="<?= base_url($product_object->aditional_information->images->resource_aditional_4->small); ?>" />
                                <?php } else { ?>

                                    <label>Sin imagen cargada</label>
                                <?php } ?>
                            </div>
                            <div class="col-lg-3">
                                <?php if (strlen($product_object->aditional_information->images->resource_aditional_4->path) > 0) { ?>
                                    <div class="col-xs-12" style="text-align: center; margin: 15% 0;">
                                        <button type="submit" onclick="deleteAditionalImage('<?= $product_object->_id; ?>','<?= $product_object->aditional_information->images->resource_aditional_4->path; ?>','resource_aditional_4')" class="btn btn-danger btn-delete"><i class="fa fa-trash-o"></i> <?= translate('delete_btn_lang'); ?></button>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-xs-12" style="text-align: left;">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                            </div>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
</div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(function() {
        $(".btn-delete").on("click", function(event) {
            event.preventDefault();
        });
    });

    function deleteAditionalImage(product_id, path_image, key_resource) {
        console.log("Delete image aditional: " + product_id);
        var url = '<?= site_url('product/delete_aditional_image'); ?>';

        $.post(url, {
            product_id: product_id,
            path_image: path_image,
            key_resource: key_resource
        }).done((resp) => {

            //resp = JSON.parse(resp);
            console.log(resp);

            window.location.href = resp;

        });
    }
</script>