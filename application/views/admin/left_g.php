<aside class="main-sidebar" style="background-color:#373736 !important;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <?php
            $url = base_url('assets/avatars/juice.png');
            if (file_exists($this->session->userdata('photo')))
                $url = base_url($this->session->userdata('photo'));
            ?>
            <div class="pull-left image">
                <img src="<?= $url; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= $this->session->userdata('name'); ?></p>
                <a href="#">En-Línea</a>
            </div>
        </div>
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

            <?php if ($this->session->userdata('role_id') == 1) { ?>

                <li>
                    <a href="<?= site_url('dashboard/index'); ?>">
                        <i class="fa fa-dashboard"></i> <span><?= translate('resume_lang'); ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('user/index'); ?>">
                        <i class="fa fa-users"></i> <span><?= translate('user_config_lang'); ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('banner/index'); ?>">
                        <i class="fa fa-cog"></i> <span><?= translate('banner_config_lang'); ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('logo/index'); ?>">
                        <i class="fa fa-cog"></i> <span><?= translate('logo_config_lang'); ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('category_product/index'); ?>">
                        <i class="fa fa-cog"></i> <span><?= translate('category_prod_config_lang'); ?></span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-cog"></i> <span><?= translate('oru_clientes_config_lang'); ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li>
                            <a href="<?= site_url('ourclient/index_description'); ?>">
                                <i class="fa fa-cog"></i> <span><?= translate('oru_clientes_des_config_lang'); ?></span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= site_url('ourclient/index_ourclient'); ?>">
                                <i class="fa fa-cog"></i> <span><?= translate('oru_clientes_list_config_lang'); ?></span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?= site_url('workus/index'); ?>">
                        <i class="fa fa-cog"></i> <span><?= translate('workus_config_lang'); ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('contact/index'); ?>">
                        <i class="fa fa-cog"></i> <span><?= translate('contact_config_lang'); ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('about/index'); ?>">
                        <i class="fa fa-cog"></i> <span><?= translate('about_config_lang'); ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('footer/index'); ?>">
                        <i class="fa fa-cog"></i> <span><?= translate('footer_config_lang'); ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('product/index'); ?>">
                        <i class="fa fa-cog"></i> <span><?= translate('product_config_lang'); ?></span>
                    </a>
                </li>
                <li>
                <a href="<?= site_url('import_products/import_index'); ?>">
                    <i class="fa fa-cog"></i> <span><?= translate('product_import_config_lang'); ?></span>
                </a>
            </li>


            <?php }  ?>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>