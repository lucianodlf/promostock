<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('footer_config_lang'); ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('edit_form_generic_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('edit_form_generic_lang'); ?></h3>
                    </div>
                    <div class="box-body">
                        <div id="alert-message" class="alert alert-danger alert-dismissable" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> <?= translate('title_alert_message_lang'); ?></h4>
                            <p></p>
                        </div>

                        <?php if ($footer_object->_id != null) {
                            $action = 'footer/update';
                        } else {
                            $action = 'footer/add';
                        } ?>

                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart($action); ?>
                        <input type="hidden" name="footer-id" value="<?= $footer_object->_id; ?>">

                        <div class="row">
                            <div class="col-lg-3">
                                <label><?= translate("footer_title_1_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" maxlength="255" class="form-control" name="title-1" placeholder="<?= translate('footer_title_1_lang'); ?>" value="<?= $footer_object->title_1; ?>" required>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <label><?= translate("footer_title_2_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" maxlength="255" class="form-control" name="title-2" placeholder="<?= translate('footer_title_2_lang'); ?>" value="<?= $footer_object->title_2; ?>" required>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <label><?= translate("footer_title_3_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" maxlength="255" class="form-control" name="title-3" placeholder="<?= translate('footer_title_3_lang'); ?>" value="<?= $footer_object->title_3; ?>" required>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <label><?= translate("footer_title_4_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" maxlength="255" class="form-control" name="title-4" placeholder="<?= translate('footer_title_4_lang'); ?>" value="<?= $footer_object->title_4; ?>" required>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-4">
                                <label><?= translate("footer_link_fb_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" maxlength="255" class="form-control" name="link-fb" placeholder="<?= translate('footer_link_fb_lang'); ?>" value="<?= $footer_object->link_fb; ?>" required>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label><?= translate("footer_link_instagram_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" maxlength="255" class="form-control" name="link-instagram" placeholder="<?= translate('footer_link_instagram_lang'); ?>" value="<?= $footer_object->link_instagram; ?>" required>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label><?= translate("footer_link_linkedin_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <input type="text" maxlength="255" class="form-control" name="link-linkedin" placeholder="<?= translate('footer_link_linkedin_lang'); ?>" value="<?= $footer_object->link_linkedin; ?>" required>
                                </div>
                            </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-12">
                                <label><?= translate("footer_text_lang"); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                    <textarea name="text" rows="4" class="form-control" placeholder="<?= translate('footer_text_lang'); ?>"><?= $footer_object->text; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12" style="text-align: left;">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                            </div>
                        </div>

                        <?= form_close(); ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(function() {

    });
</script>