<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('our_clients_lang'); ?>
            <small><?= translate('oru_clientes_config_lang'); ?></small>
            | <a href="<?= site_url('ourclient/index_ourclient_add'); ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?= translate('add_item_lang'); ?>
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('oru_clientes_list_config_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?= translate('oru_clientes_list_config_lang'); ?></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <?= get_message_from_operation(); ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?= translate("image_table_title_lang"); ?></th>
                                    <th><?= translate("name_table_title_lang"); ?></th>
                                    <th><?= translate("comment_table_title_lang"); ?></th>
                                    <th><?= translate("status_table_title_lang"); ?></th>
                                    <th><?= translate("actions_table_title_lang"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($all_ourclients) { ?>
                                    <?php foreach ($all_ourclients as $item) { ?>
                                        <tr>
                                            <td>
                                                <img style="margin:0 auto; width: 50%;" class="img img-rounded img-responsive" src="<?= base_url($item->resource); ?>" />
                                            </td>
                                            <td><?= $item->name; ?></td>
                                            <td><?= $item->comment; ?></td>
                                            <td>
                                                <?php if ($item->is_active == 1) { ?>
                                                    <h5 class="text-green"><i class="fa fa-check"></i> Activo</h5>
                                                <?php } ?>
                                                <?php if ($item->is_active == 0) { ?>
                                                    <h5 class="text-yellow"><i class="fa fa-ban"></i> Inactivo</h5>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <!-- Single button -->
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Acciones <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a class="text-blue" href="<?= site_url('ourclient/index_ourclient_update/' . $item->_id); ?>"><i class="fa fa-edit"></i> <?= translate("edit_btn_lang"); ?></a></li>
                                                        <?php if ($item->is_active == 1) { ?>
                                                            <li><a class="text-yellow" href=<?= site_url("ourclient/update_status_ourclient/0/{$item->_id}"); ?>><i class="fa fa-ban"></i> <?= translate("deactive_btn_lang"); ?></a></li>
                                                        <?php } ?>
                                                        <?php if ($item->is_active == 0) { ?>
                                                            <li><a class="text-green" href=<?= site_url("ourclient/update_status_ourclient/1/{$item->_id}"); ?>><i class="fa fa-check"></i> <?= translate("active_btn_lang"); ?></a></li>
                                                        <?php } ?>
                                                        <li><a class="text-red" data-toggle="modal" data-target="#modal-delete" data-action="delete_ourclient" data-id="<?= $item->_id; ?>" href=""><i class="fa fa-remove"></i> <?= translate("delete_btn_lang"); ?></a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th><?= translate("image_table_title_lang"); ?></th>
                                    <th><?= translate("name_table_title_lang"); ?></th>
                                    <th><?= translate("comment_table_title_lang"); ?></th>
                                    <th><?= translate("status_table_title_lang"); ?></th>
                                    <th><?= translate("actions_table_title_lang"); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>
    $(function() {
        $(".modal-title").text('<?= translate('title_alert_ourclient_delete'); ?>');
        $(".modal-body p").text('<?= translate('message_alert_ourclient_delete'); ?>');

        $("#example1").DataTable({
            "order": [
                [1, "asc"]
            ],
            "ordering": true,
            "columnDefs": [{
                "width": "5%",
                "className": "text-center",
                "targets": "_all",
            }],
        });

    });
</script>