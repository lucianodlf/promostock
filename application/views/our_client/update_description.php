<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= translate('our_clients_lang'); ?>
            <small><?= translate('edit_des_our_clients_lang'); ?></small>
            | <a href="<?= site_url('ourclient/index_description'); ?>" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i> <?= translate('back_lang'); ?>
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= site_url('dashboard/index'); ?>"><i class="fa fa-dashboard"></i> <?= translate('resume_lang'); ?></a></li>
            <li class="active"><?= translate('edit_des_our_clients_lang'); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= translate('edit_des_our_clients_lang'); ?></h3>
                    </div>
                    <div class="box-body">
                        <div id="alert-message" class="alert alert-danger alert-dismissable" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> <?= translate('title_alert_message_lang'); ?></h4>
                            <p></p>
                        </div>

                        <?= get_message_from_operation(); ?>

                        <?= form_open_multipart("ourclient/update_description"); ?>

                        <input type="hidden" value="active" name="is_active">
                        <input id="id-our-clients" type="hidden" name="id-our-clients" value="<?= $our_cli_des_object->_id; ?>">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label><?= translate("our_clients_des_title_label_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <input type="text" maxlength="255" class="form-control" name="title" placeholder="<?= translate('our_clients_des_title_ph_lang'); ?>" value="<?= $our_cli_des_object->title; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label><?= translate("our_clients_des_description_label_lang"); ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-text-height"></i></span>
                                            <textarea name="description" rows="5" class="form-control"><?= trim($our_cli_des_object->description); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: left;">
                                        <button id="btn-submit-form" type="submit" class="btn btn-primary"><i class="fa fa-check-square"></i> <?= translate('save_btn_lang'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <?= form_close(); ?>
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">

</script>