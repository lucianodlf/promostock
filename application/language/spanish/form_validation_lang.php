<?php

$lang['required']			= "El campo <strong>%s</strong> es obligatorio.";
$lang['isset']				= "El campo <strong>%s</strong> debe contener un valor.";
$lang['valid_email']		= "El campo <strong>%s</strong> debe contener una dirección de correo válida.";
$lang['valid_emails']		= "El campo <strong>%s</strong> debe contener todas las direcciones de correo válidas.";
$lang['valid_url']			= "El campo <strong>%s</strong> debe contener una URL válida.";
$lang['valid_ip']			= "El campo <strong>%s</strong> debe contener una dirección IP válida.";
$lang['min_length']			= "El campo <strong>%s</strong> debe contener al menos <strong>%s</strong> caracteres de longitud.";
$lang['max_length']			= "El campo <strong>%s</strong> no debe exceder los <strong>%s</strong> caracteres de longitud.";
$lang['exact_length']		= "El campo <strong>%s</strong> debe tener exactamente <strong>%s</strong> carácteres.";
$lang['alpha']				= "El campo <strong>%s</strong> sólo puede contener carácteres alfabéticos.";
$lang['alpha_numeric']		= "El campo <strong>%s</strong> sólo puede contener carácteres alfanuméricos.";
$lang['alpha_dash']			= "El campo <strong>%s</strong> sólo puede contener carácteres alfanuméricos, guiones bajos '_' o guiones '-'.";
$lang['numeric']			= "El campo <strong>%s</strong> sólo puede contener números.";
$lang['is_numeric']			= "El campo <strong>%s</strong> sólo puede contener carácteres numéricos.";
$lang['integer']			= "El campo <strong>%s</strong> debe contener un número entero.";
$lang['regex_match']		= "El campo <strong>%s</strong> no tiene el formato correcto.";
$lang['matches']			= "El campo <strong><strong>%s</strong></strong> no concuerda con el campo <strong>%s</strong> .";
$lang['is_natural']			= "El campo <strong>%s</strong> debe contener sólo números positivos.";
$lang['is_natural_no_zero']	= "El campo <strong>%s</strong> debe contener un número mayor que 0.";
$lang['decimal']			= "El campo <strong>%s</strong> debe contener un número decimal.";
$lang['less_than']			= "El campo <strong>%s</strong> debe contener un número menor que <strong>%s</strong>.";
$lang['greater_than']		= "El campo <strong>%s</strong> debe contener un número mayor que <strong>%s</strong>.";
/* Added after 2.0.2 */
$lang['is_unique'] 			= "El campo <strong>%s</strong> debe contener un valor único.";

/* End of file form_validation_lang.php */
/* Location: ./system/language/spanish/form_validation_lang.php */