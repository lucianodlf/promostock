<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Define MONGO_DB configuration for PRODUCTION enviroment
*/
$config['mongo_db']['active_config_group'] = 'default';

/**
 *  Connection settings for #default group.
 */
$config['mongo_db']['default'] = [
	'settings' => [
		'auth'             => false,
		'debug'            => TRUE,
		'return_as'        => 'array',
		'auto_reset_query' => TRUE
	],

	'connection_string' => '',

	'connection' => [
		'host'          => 'localhost',
		'port'          => '27017',
		'user_name'     => '',
		'user_password' => '',
		'db_name'       => 'promostock',
		'db_options'    => []
	],
	'driver' => []
];
