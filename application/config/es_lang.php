<?php
//Form validation
/**
 * application
 * 	+---languague
 * 		+---form_validation_lang.php
 */

$config['site_title_lang'] = "PromoStock";

// Para uso de mensajes de validacion por fuera de la validacion
// integrada de formulario
$config['email_not_unique_lang'] = "El Email ya esta siendo utilizado.";
$config['password_incorrect_lang'] = "La contraseña actual es invalida.";


//Login
$config['login_app'] = "Autenticarse en la aplicación";
$config['email_lang'] = "E-mail";
$config['password_lang'] = "Contraseña";
$config['entrar_lang'] = "Entrar";
$config['i_forgot_password_lang'] = "Yo olvidé mi contraseña";


//Message reponses
$config['autenticacion_error_lang'] = "Error de autenticación";
$config['unauthorized_lang'] = "Acceso no autorizado";
$config['message_error'] = "Lo sentimos, parece que ocurrio un error en la operación. Intente nuevamente y si el problema persiste comuniquese con su administrador.<br> Muchas gracias";
$config['title_alert_message_lang'] = "Atencion!";
$config['message_file_tipe_not_accept'] = "Ups! El tipo de archivo no es aceptado! <br> Recuerde que solo puede cargar imagenes de tipo (".     implode(', ',ALLOW_EXTENSION_UPLOAD_IMAGE) .")";
$config['message_file_size_exceeded'] = "Ups! El arhcivo exede el tamaño aceptado! <br> Recurde que puede cargar archivo con un maximo de " . (MAX_FILE_IMAGE_UPLOAD / 1048576) . " MB";
$config['message_file_size_exceeded_php_ini'] = "Ups! El arhcivo exede el tamaño aceptado por la configuracion del servidor.";

// -> users
$config['message_save_user_ok'] = "Usuario guardado correctamente";
$config['message_delete_user_ok'] = "Usuario eliminado correctamente";
$config['message_update_user_ok'] = "Usuario actualizado correctamente";

// -> banners
$config['message_save_banner_ok'] = "Banner guardado correctamente";
$config['message_delete_banner_ok'] = "Banner eliminado correctamente";
$config['message_update_banner_ok'] = "Banner actualizado correctamente";

// -> logos
$config['message_save_logo_ok'] = "Logo guardado correctamente";
$config['message_delete_logo_ok'] = "Logo eliminado correctamente";
$config['message_update_logo_ok'] = "Logo actualizado correctamente";
$config['message_max_limit_active_logos'] = "Solo pueden exisitr 3 logos activos";
$config['message_limit_logo_for_section'] = "Solo pude existir un logo activo por sección";

// -> category products
$config['message_save_category_prod_ok'] = "Categoría guardada correctamente";
$config['message_delete_category_prod_ok'] = "Categoría eliminada correctamente";
$config['message_update_category_prod_ok'] = "Categoría actualizada correctamente";

// -> Our Clients
$config['message_save_des_our_clients_ok'] = "Descripción guardada correctamente";
$config['message_delete_des_our_clients_ok'] = "Descripción eliminada correctamente";
$config['message_update_des_our_clients_ok'] = "Descripción actualizada correctamente";
$config['message_save_our_clients_ok'] = "Cliente guardado correctamente";
$config['message_delete_our_clients_ok'] = "Cliente eliminado correctamente";
$config['message_update_our_clients_ok'] = "Cliente actualizado correctamente";
$config['message_max_description_our_clients'] = "Sol puede cargar una descripción";

// -> Form generico
$config['message_save_form_ok'] = "Datos guardados correctamente";
$config['message_update_form_ok'] = "Datos actualizados correctamente";

// -> Products
$config['message_save_product_ok'] = "Producto guardado correctamente";
$config['message_delete_product_ok'] = "Producto eliminado correctamente";
$config['message_update_product_ok'] = "Producto actualizado correctamente";

// -> send email
$config['message_change_email_lang'] = "La contraseña ha sido cambiada correctamente";
$config['message_user_not_exist_lang'] = "No existe ningun usuario con el correo electronico especificado";

// -> create review
$config['message_error_create_review'] = "Disculpe, parece que ocurrio un error al crear el comentario, por favor vuelva a intentarlo.";
$config['message_save_review_ok'] = "Comentario guardado!";


//Dashboard
$config['date_lang'] = "Fecha";
$config['name_lang'] = "Nombre";
$config['email_lang'] = "E-mail";
$config['phone_lang'] = "Teléfono";
$config['message_lang'] = "Mensaje";
$config['dashboard_lang'] = "Panel de control";


//Section configuration title
$config['banner_config_lang'] = "Banners";
$config['user_config_lang'] = "Usuarios";
$config['logo_config_lang'] = "Logos";
$config['category_prod_config_lang'] = "Categorías de Productos";
$config['oru_clientes_config_lang'] = "Nuestros Clientes";
$config['oru_clientes_des_config_lang'] = "Descripción";
$config['oru_clientes_list_config_lang'] = "Lista de Clientes";
$config['workus_config_lang'] = "Trabaja con nosotros";
$config['contact_config_lang'] = "Contacto";
$config['about_config_lang'] = "Sobre Nosotros";
$config['footer_config_lang'] = "Footer";
$config['product_config_lang'] = "Productos";
$config['product_import_config_lang'] = "Importar productos";


//Admin header profile
$config['profile_lang'] = "Datos de perfil";
$config['credential_lang'] = "Credenciales";
$config['sign_out_lang'] = "Cerrar sesión";
$config['edit_profile_lang'] = "Editar perfil";
$config['edit_credential_lang'] = "Editar credenciales";


//Admin body
$config['resume_lang'] = "Pizarra resumen";
$config['add_item_lang'] = "Adicionar elemento";
$config['back_lang'] = "Atras";


//Admin users
$config['users_lang'] = "Usuarios";
$config['user_list_lang'] = "Lista de usuarios";
$config['role_admin_lang'] = "Administrador";
$config['role_user_lang'] = "Usuario";
$config['new_user_lang'] = "Nuevo Usuario";
$config['edit_user_lang'] = "Modificar Usuario";

//Admin Products
$config['new_product_lang'] = "Nuevo Producto";
$config['product_list_lang'] = "Lista de Productos";
$config['edit_product_lang'] = "Modificar Producto";
$config['product_lang'] = "Producto";
$config['aditional_images_lang'] = "Editar imagenes adicionales";

//Admin banners
$config['new_banner_lang'] = "Nuevo Banner";
$config['banner_list_lang'] = "Lista de banner";
$config['edit_banner_lang'] = "Modificar Banner";
$config['banners_lang'] = "Banner";
$config['banner_type_1_lang'] = "Foto unica";
$config['banner_type_2_lang'] = "Galeria";
$config['banner_type_3_lang'] = "Video";


//Admin Logos
$config['new_logo_lang'] = "Nuevo Logo";
$config['logo_list_lang'] = "Lista de Logo";
$config['edit_logo_lang'] = "Modificar Logo";
$config['logos_lang'] = "Logo";

//Admin Category product
$config['new_category_prod_lang'] = "Nueva Categoría";
$config['category_prod_list_lang'] = "Lista de Categorías";
$config['edit_category_prod_lang'] = "Modificar Categoría";
$config['category_prod_lang'] = "Categoría de producto";


//Admin Our Clients
$config['new_des_our_clients_lang'] = "Nueva Descripción";
$config['new_our_clients_lang'] = "Nuevo Cliente";
$config['our_clients_list_lang'] = "Lista de Clientes";
$config['edit_our_clients_lang'] = "Modificar Cliente";
$config['edit_des_our_clients_lang'] = "Modificar Descripción";
$config['our_clients_lang'] = "Nuestros Clientes";





//Admin Form Generic
$config['edit_form_generic_lang'] = "Editar formulario";

//Form contact
$config['contact_label_latitude_lang'] = "Latitud (Mapa 1)";
$config['contact_label_longuitude_lang'] = "Longitud (Mapa 1)";
$config['contact_label_latitude_2_lang'] = "Latitud (Mapa 2)";
$config['contact_label_longuitude_2_lang'] = "Longitud (Mapa 2)";
$config['contact_label_text_lang'] = "Texto";
$config['contact_label_name_lang'] = "Su Nombre";
$config['contact_label_email_lang'] = "Su Email";
$config['contact_label_phone_lang'] = "Su Teléfono";
$config['contact_label_subject_lang'] = "Asunto";
$config['contact_label_message_lang'] = "Mensaje";


//Form footer
$config['footer_title_1_lang'] = "Titulo 1";
$config['footer_title_2_lang'] = "Titulo 2";
$config['footer_title_3_lang'] = "Titulo 3";
$config['footer_title_4_lang'] = "Titulo 4";
$config['footer_link_fb_lang'] = "Link Facebook";
$config['footer_link_instagram_lang'] = "Link Instagram";
$config['footer_link_linkedin_lang'] = "Link Linkedin";
$config['footer_text_lang'] = "Texto";


//Form About
$config['about_label_text_first_lang'] = "Texto Primario";
$config['about_label_text_second_lang'] = "Texto Secundario";
$config['about_label_text_vision_lang'] = "Visión";
$config['about_label_text_mision_lang'] = "Misión";
$config['about_label_number_count_1_lang'] = "Contador 1 - Cantidad";
$config['about_label_number_count_2_lang'] = "Contador 2 - Cantidad";
$config['about_label_number_count_3_lang'] = "Contador 3 - Cantidad";
$config['about_label_title_count_1_lang'] = "Contador 1 - Titulo";
$config['about_label_title_count_2_lang'] = "Contador 2 - Titulo";
$config['about_label_title_count_3_lang'] = "Contador 3 - Titulo";
$config['about_label_img_subh_lang'] = "Imagen - Subheader";
$config['about_label_img_first_lang'] = "Imagen - Primaria";


//Form Product
$config['product_label_code_lang'] = "Código";
$config['product_label_name_lang'] = "Nombre";
$config['product_label_category_lang'] = "Categória";
$config['product_label_description_lang'] = "Descripción";
$config['product_label_price_f_lang'] = "Precio Final";
$config['product_label_stock_quantity_lang'] = "Stock (cantidad)";
$config['product_label_main_image_lang'] = "Imagen principal";
$config['product_label_import_file_lang'] = "Listado a importar (xlsx)";
$config['product_label_status_lang'] = "Resumen - Importación de productos";
$config['product_label_import_zip_lang'] = "Images a importar (zip)";
$config['product_label_status_zip_lang'] = "Resumen - Importación de imagenes";
$config['product_label_starts_init_lang'] = "Puntuación inicial";
$config['product_option_starts_default_lang'] = "Sin puntuación inicial";
$config['product_label_is_deal_lang'] = "Es Oferta";
$config['product_label_is_show_deal_banner_lang'] = "Mostrar en banner de Oferta";
$config['product_label_aditional_image_lang'] = "Imagen adicional";



//Generic Form elements (form_function_name_lang)
$config['form_label_title_lang'] = "Titulo";
$config['form_label_title_2_lang'] = "Titulo 2";
$config['form_ph_title_lang'] = "Aqui un titulo ...";
$config['form_label_subtitle_lang'] = "Subtitulo";
$config['form_ph_subtitle_lang'] = "Aqui un subtitulo ...";
$config['form_label_description_lang'] = "Descripción";
$config['form_ph_description_lang'] = "Aqui una descripción ...";
$config['form_label_image_lang'] = "Imagen";
$config['form_label_phone_lang'] = "Teléfonos";
$config['form_ph_phone_lang'] = "Ej: +359(2)2903336, +359(2)3237720, +359(2)3238002";
$config['form_label_email_lang'] = "E-mail";
$config['form_ph_email_lang'] = "ejemplo@ejemplo.com";
$config['form_label_address_lang'] = "Dirección";
$config['form_label_address_2_lang'] = "Dirección 2";
$config['form_ph_address_lang'] = "Cale 0011 ....";
$config['text_option_yes'] = "Si";
$config['text_option_not'] = "No";


//Admin Our Clients form
$config['our_clients_name_label_lang'] = "Nombre";
$config['our_clients_name_ph_lang'] = "Coleman ...";
$config['our_clients_comment_label_lang'] = "Comentario";
$config['our_clients_comment_ph_lang'] = "Estamos muy satisfechos con el servicio de PromoStock ...";
$config['our_clients_des_title_label_lang'] = "Titulo";
$config['our_clients_des_title_ph_lang'] = "Alguno de nuestros clientes ...";
$config['our_clients_des_description_label_lang'] = "Descripción";
$config['our_clients_des_description_ph_lang'] = "Cuidamos nuestros clientes ...";


//Admin Logos form
$config['logo_text_label_lang'] = "Texto Altenativo";
$config['logo_text_ph_lang'] = "PromoStock ...";


//Admin Category products form
$config['category_prod_text_label_lang'] = "Titulo";
$config['category_prod_text_ph_lang'] = "Esferos ...";


//Admin banners form
$config['title_label_lang'] = "Titulo";
$config['title_ph_lang'] = "Nuevos productos...";
$config['text_label_lang'] = "Texto";
$config['text_ph_lang'] = "Tenemos todo lo que necesitas...";
$config['type_label_lang'] = "Tipo";
$config['image_label_lang'] = "Imagen";
$config['video_label_lang'] = "URL Video";
$config['video_ph_lang'] = "https://www.youtube.com/watch?v=rZb5LwEdMsY&list=RDw7_ahe1OKnI&index=2";
$config['order_label_lang'] = "Orden";
$config['order_ph_lang'] = "1";
$config['link_label_lang'] = "Url (Boton)";
$config['link_ph_lang'] = "http://www.promostock.com.ec/temporada.html ...";
$config['link_btn_name_lang'] = "Titulo (Boton)";
$config['text_align_label_lang'] = "Alineacion del textos";
$config['color_picker_label_lang'] = "Color de textos";

//-------------------- Admin users form
$config['name_label_lang'] = "Nombre Completo";
$config['name_ph_lang'] = "Ej: Luciano ...";
$config['phone_label_lang'] = "Teléfono";
$config['phone_ph_lang'] = "9899561810 ...";
$config['role_label_lang'] = "Rol";
$config['pwd_label_lang'] = "Contraseña";
$config['pwd_ph_lang'] = "#Co44xz ...";
$config['repeat_pwd_label_lang'] = "Repetir contraseña";
$config['email_label_lang'] = "E-mail";
$config['email_ph_lang'] = "ejemplo@ejemplo.com ...";
$config['photo_label_lang'] = "Foto";
$config['current_pwd_label_lang'] = "Contraseña Actual";
$config['new_pwd_label_lang'] = "Contraseña Nueva";
$config['repeat_new_pwd_label_lang'] = "Repetir Contraseña Nueva";
//-------------------------------------------------------------------


//-------------------- Table title
$config['actions_table_title_lang'] = "Acciones";
$config['name_table_title_lang'] = "Nombre";
$config['code_table_title_lang'] = "Código";
$config['role_table_title_lang'] = "Rol";
$config['email_table_title_lang'] = "E-mail";
$config['title_table_title_lang'] = "Titulo";
$config['description_table_title_lang'] = "Descripción";
$config['text_table_title_lang'] = "Subtitulo";
$config['type_table_title_lang'] = "Tipo";
$config['status_table_title_lang'] = "Estado";
$config['section_table_title_lang'] = "Sección";
$config['image_table_title_lang'] = "Imagen";
$config['order_table_title_lang'] = "Orden";
$config['btn_link_table_title_lang'] = "Boton(Link)";
$config['comment_table_title_lang'] = "Comentario";
$config['category_table_title_lang'] = "Categória";
//-------------------------------------------------------------------


//--------------------- Buttons
$config['actions_btn_lang'] = "Acciones";
$config['edit_btn_lang'] = "Editar";
$config['delete_btn_lang'] = "Eliminar";
$config['save_btn_lang'] = "Guardar";
$config['cancel_btn_lang'] = "Cancelar";
$config['active_btn_lang'] = "Activar";
$config['deactive_btn_lang'] = "Desactivar";
$config['new_btn_lang'] = "Nuevo";
/**************************************************************************************************/


//--------------------- Modal delete
$config['title_alert_user_delete'] = "¿Eliminar Usuario?";
$config['title_alert_banner_delete'] = "¿Eliminar Banner?";
$config['title_alert_logo_delete'] = "¿Eliminar Logo?";
$config['title_alert_category_prod_delete'] = "¿Eliminar Categoría?";
$config['title_alert_ourclient_delete'] = "¿Eliminar Cliente?";
$config['title_alert_product_delete'] = "¿Eliminar Producto?";

$config['message_alert_user_delete'] = "¿Confirma que desea eliminar el usuario?";
$config['message_alert_banner_delete'] = "¿Confirma que desea eliminar el banner?";
$config['message_alert_logo_delete'] = "¿Confirma que desea eliminar el logo?";
$config['message_alert_category_prod_delete'] = "¿Confirma que desea eliminar la categoría?";
$config['message_alert_ourclient_delete'] = "¿Confirma que desea eliminar el cliente?";
$config['message_alert_product_delete'] = "¿Confirma que desea eliminar el producto?";
/**************************************************************************************************/

$config['cancel'] = "Cancelar";
$config['confirm'] = "Confirmar";
$config['active'] = "Activar";
$config['deactive'] = "Desactivar";
$config['confirm'] = "Confirmar";


//-------------------- Home - Front
$config['title_deal_section'] = "Ofertas del día";
$config['title_popular_section'] = "Más populares";
$config['title_new_section'] = "Nuevos productos";
$config['title_most_view_section'] = "Más vistos";
/**************************************************************************************************/


//-------------------- Products review - Front
$config['ph_review_user_name'] = "Ingrese su nombre";
$config['ph_review_user_email'] = "Ingrese su email";
$config['ph_review_text'] = "Ingrese su comentario";
$config['bt_review_submit'] = "Comentar";

/**************************************************************************************************/

//Message send Email
$config['msg_subject_recover_pwd'] = "Recuperación de contraseña";
?>