<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Define CONFIG configuration for TESTING enviroment
*/

/* Local config */
// $config['base_url'] = 'http://test.promostock.local/';

/* Remote config */
$config['base_url'] = 'http://test.promostock.datalabcenter.com/';

//$config['index_page'] = '';

//$config['uri_protocol']	= 'REQUEST_URI';

$config['log_threshold'] = 1;
$config['log_file_extension'] = 'log';


