<?php

class Contact extends CI_Controller
{

    private const WIDTH = 1873;
    private const HEIGHT = 350;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Contact_model', 'contact');

        @session_start();
        $this->init_form_validation();
    }



    public function index()
    {

        $this->validate_rol([1]);

        $contact_object = $this->contact->get();
        
        if(!$contact_object){
            
            $contact_object = (object)[
                '_id' => null,
                'title' => '',
                'email' => '',
                'phone' => '',
                'address' => '',
                'location' => (object)[
                    'latitude' => 0,
                    'longuitude' => 0,
                    'latitude_2' => 0,
                    'longuitude_2' => 0,
                ],
                'resource' => null
            ];
        }

        $data['contact_object'] = $contact_object;
        $this->load_view_admin_g("contact/index", $data);
    }



    public function add()
    {
        $this->validate_rol([1]);

        $title = $this->input->post('title');
        $title_2 = $this->input->post('title-2');
        $text = $this->input->post('text');
        $phone = $this->input->post('phone');
        $address = $this->input->post('address');
        $address_2 = $this->input->post('address-2');
        $latitude = $this->input->post('latitude');
        $longuitude = $this->input->post('longuitude');
        $latitude_2 = $this->input->post('latitude-2');
        $longuitude_2 = $this->input->post('longuitude-2');
        $email = $this->input->post('email');

        $this->form_validation->set_rules('title', translate('form_label_title_lang'), 'trim|required');
        $this->form_validation->set_rules('title-2', translate('form_label_title_2_lang'), 'trim|required');
        $this->form_validation->set_rules('text', translate('contact_label_text_lang'), 'trim');
        $this->form_validation->set_rules('phone', translate('form_label_phone_lang'), 'trim|required');
        $this->form_validation->set_rules('address', translate('form_label_address_lang'), 'trim|required');
        // $this->form_validation->set_rules('latitude', translate('contact_label_latitude_lang'), 'trim|required');
        // $this->form_validation->set_rules('longuitude', translate('contact_label_longuitude_lang'), 'trim|required');
        $this->form_validation->set_rules('email', translate('form_label_email_lang'), 'trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            redirect("contact/index", 'location', 301);
        }

        $data = [
            'title' => $title,
            'title_2' => $title_2,
            'text' => $text,
            'phone' => $phone,
            'address' => $address,
            'address_2' => $address_2,
            'email' => $email,
            'resource' => null,
            'location' => [
                'latitude' => (float) $latitude,
                'longuitude' => (float) $longuitude,
                'latitude_2' => (float) $latitude_2,
                'longuitude_2' => (float) $longuitude_2,
            ],
            'is_active' => 1
        ];

        $name_file = $_FILES['archivo']['name'];
        $separado = explode('.', $name_file);
        $ext = end($separado); // me quedo con la extension
        $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
        $allow_extension = in_array($ext, $allow_extension_array);

        // $_FILES['archivo']['error'] == 4 // si no se subio archivo

        if ($allow_extension) {

            $result = save_image_from_post('archivo', './uploads/contact', time(), $this::WIDTH, $this::HEIGHT);
            if ($result[0]) {

                $data['resource'] = $result[1];

                $this->contact->create($data);
                $this->response->set_message(translate("message_save_form_ok"), ResponseMessage::SUCCESS);
                redirect("contact/index", "location", 301);
            } else {

                if ($_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE) $result[1] = translate('message_file_size_exceeded_php_ini');

                $this->response->set_message($result[1], ResponseMessage::ERROR);
                redirect("contact/index", "location", 301);
            }
        } else {

            $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
            redirect("contact/index/", "location", 301);
        }
    }



    public function update()
    {
        $this->validate_rol([1]);

        $title = $this->input->post('title');
        $title_2 = $this->input->post('title-2');
        $text = $this->input->post('text');
        $phone = $this->input->post('phone');
        $address = $this->input->post('address');
        $address_2 = $this->input->post('address-2');
        $latitude = $this->input->post('latitude');
        $longuitude = $this->input->post('longuitude');
        $latitude_2 = $this->input->post('latitude-2');
        $longuitude_2 = $this->input->post('longuitude-2');
        $email = $this->input->post('email');

        $contact_id = $this->input->post('contact-id');
        $old_file = $this->input->post('old-file');

        $this->form_validation->set_rules('title', translate('form_label_title_lang'), 'trim|required');
        $this->form_validation->set_rules('title-2', translate('form_label_title_2_lang'), 'trim|required');
        $this->form_validation->set_rules('text', translate('contact_label_text_lang'), 'trim');
        $this->form_validation->set_rules('phone', translate('form_label_phone_lang'), 'trim|required');
        $this->form_validation->set_rules('address', translate('form_label_address_lang'), 'trim|required');
        // $this->form_validation->set_rules('latitude', translate('contact_label_latitude_lang'), 'trim|required');
        // $this->form_validation->set_rules('longuitude', translate('contact_label_longuitude_lang'), 'trim|required');
        $this->form_validation->set_rules('email', translate('form_label_email_lang'), 'trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            redirect("contact/index", 'location', 301);
        }

        $data = [
            'title' => $title,
            'title_2' => $title_2,
            'text' => $text,
            'phone' => $phone,
            'address' => $address,
            'address_2' => $address_2,
            'email' => $email,
            'location' => [
                'latitude' => (float) $latitude,
                'longuitude' => (float) $longuitude,
                'latitude_2' => (float) $latitude_2,
                'longuitude_2' => (float) $longuitude_2,
            ],
            'is_active' => 1
        ];

        $update_file = FALSE;

        $name_file = $_FILES['archivo']['name'];
        $ofile_name = explode('/', $old_file);

        if (end($ofile_name) != $name_file and $name_file != null) {
            $update_file = TRUE;
        }

        if (!$update_file) {

            $r = $this->contact->update($contact_id, $data);

            if ($r === false) {

                $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                redirect("contact/index/", "location", 301);
            } else {

                $this->response->set_message(translate("message_update_form_ok"), ResponseMessage::SUCCESS);
                redirect("contact/index/", "location", 301);
            }
        } else {

            $separado = explode('.', $name_file);
            $ext = end($separado); // me quedo con la extension
            $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
            $allow_extension = in_array($ext, $allow_extension_array);

            if ($allow_extension) {

                $result = save_image_from_post('archivo', './uploads/contact', time(), $this::WIDTH, $this::HEIGHT);
                if ($result[0]) {
                    if ($old_file != null) {
                        unlink($old_file);
                    }

                    $data['resource'] = $result[1];

                    $r = $this->contact->update($contact_id, $data);
                    if ($r > 0) {
                        $this->response->set_message(translate("message_update_form_ok"), ResponseMessage::SUCCESS);
                        redirect("contact/index/", "location", 301);
                    } else {
                        $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                        redirect("contact/index/", "location", 301);
                    }
                } else {

                    if ($_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE) $result[1] = translate('message_file_size_exceeded_php_ini');

                    $this->response->set_message($result[1], ResponseMessage::ERROR);
                    redirect("contact/index/", "location", 301);
                }
            } else {

                $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
                redirect("contact/index/", "location", 301);
            }
        }
    }


    public function send_email(){

        $first_name =  $this->input->post('first-name');
        $email =  trim($this->input->post('email'));
        $phone =  $this->input->post('phone');
        $subject =  $this->input->post('subject');
        $description =  $this->input->post('description');

        $this->form_validation->set_rules('first-name', translate('contact_label_name_lang'), 'trim|required');
        $this->form_validation->set_rules('email', translate('contact_label_email_lang'), 'trim|required');
        $this->form_validation->set_rules('phone', translate('contact_label_phone_lang'), 'trim|required');
        $this->form_validation->set_rules('subject', translate('contact_label_subject_lang'), 'trim|required');
        $this->form_validation->set_rules('description', translate('contact_label_message_lang'), 'trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            redirect("contact_front", 'location', 301);
        }

        $new_contact_db = [
            'name' => (string) $first_name,
            'email' => (string) $email,
            'phone' => (string) $phone,
            'subject' => (string) $subject,
            'message' => (string) $description,
            'for_subcriber' => (int) 0,
            'create_at' => time()
        ];
        
        $this->contact->add_new_contact($new_contact_db);

        $this->load->library('email');

        $this->load->config('email');

        $this->email->initialize();

        $this->email->from($this->email->smtp_user, 'PromoStock');
        
        //TODO: Completar mensaje de notificacion y modificar direcciones.
        $this->email->to($this->config->item('email_to_contactus'));

        $this->email->subject('Notificacion - Formulario de Contacto');
        $mensaje = "<h2>Estimado,<br>";
        $mensaje .= "han enviado una solicitud de contacto con los siguientes datos:</h2><br>";
        $mensaje .= "<strong>Nombre:</strong> $first_name <br>";
        $mensaje .= "<strong>E-mail:</strong> $email <br>";
        $mensaje .= "<strong>Teléfono:</strong> $phone <br>";
        $mensaje .= "<strong>Asunto:</strong> $subject <br>";
        $mensaje .= "<strong>Mensaje:</strong><em>$description</em><br>";
        
        $this->email->message($mensaje);

        $this->email->send();

        $this->response->set_message('Su mensaje ha sido enviado', ResponseMessage::SUCCESS);
        redirect("contact_front");
    }

}
