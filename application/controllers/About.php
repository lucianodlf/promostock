<?php

class About extends CI_Controller
{

    private const WIDTH = 1873;
    private const HEIGHT = 350;

    private const WIDTH_IMG_FIRST = 1000;
    private const HEIGHT_IMG_FIRST = 1000;

    private $about_default = [];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('About_model', 'about');

        @session_start();
        $this->init_form_validation();

        $this->about_default = (object)[
            '_id' => 0,
            'text_first' => null,
            'text_second' => null,
            'text_vision' => null,
            'number_count_1' => null,
            'number_count_2' => null,
            'number_count_3' => null,
            'title_count_1' => null,
            'title_count_2' => null,
            'title_count_3' => null,
            'text_mision' => null,
            'resource' => null,
            'resource_2' => null
        ];
    }



    public function index()
    {

        $this->validate_rol([1]);

        $about_object = $this->about->get();
        
        if(!$about_object){
            $about_object = $this->about_default;
        }

        $data['about_object'] = $about_object;
        $this->load_view_admin_g("about/index", $data);
    }



    public function add()
    {
        $this->validate_rol([1]);

        $text_first = $this->input->post('text-first');
        $text_mision = $this->input->post('text-mision');
        $text_vision = $this->input->post('text-vision');
        $text_second = $this->input->post('text-second');
        $number_count_1 = $this->input->post('number-count-1');
        $number_count_2 = $this->input->post('number-count-2');
        $number_count_3 = $this->input->post('number-count-3');
        $title_count_1 = $this->input->post('title-count-1');
        $title_count_2 = $this->input->post('title-count-2');
        $title_count_3 = $this->input->post('title-count-3');


        // $this->form_validation->set_rules('title', translate('form_label_title_lang'), 'trim|required');
        // $this->form_validation->set_rules('phone', translate('form_label_phone_lang'), 'trim|required');
        // $this->form_validation->set_rules('address', translate('form_label_address_lang'), 'trim|required');
        // $this->form_validation->set_rules('latitude', translate('contact_label_latitude_lang'), 'trim|required');
        // $this->form_validation->set_rules('longuitude', translate('contact_label_longuitude_lang'), 'trim|required');
        // $this->form_validation->set_rules('email', translate('form_label_email_lang'), 'trim|required');

        // if ($this->form_validation->run() === FALSE) {
        //     $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
        //     redirect("about/index", 'location', 301);
        // }

        $data = [
            'text_first' => $text_first,
            'text_second' => $text_second,
            'text_vision' => $text_vision,
            'number_count_1' => $number_count_1,
            'number_count_2' => $number_count_2,
            'number_count_3' => $number_count_3,
            'title_count_1' => $title_count_1,
            'title_count_2' => $title_count_2,
            'title_count_3' => $title_count_3,
            'text_mision' => $text_mision,
            'resource' => null
        ];

        $name_file = $_FILES['archivo']['name'];
        $name_file_2 = $_FILES['archivo_2']['name'];
        // $separado = explode('.', $name_file);
        // $ext = end($separado); // me quedo con la extension
        $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
        $allow_extension = in_array(end(explode('.', $name_file)) , $allow_extension_array);
        $allow_extension_2 = in_array(end(explode('.', $name_file_2)) , $allow_extension_array);

        if ($allow_extension && $allow_extension_2) {

            // Resource 1
            $result = save_image_from_post('archivo', './uploads/about', time(), $this::WIDTH, $this::HEIGHT);
            $result_2 = save_image_from_post('archivo_2', './uploads/about', time(), $this::WIDTH_IMG_FIRST, $this::HEIGHT_IMG_FIRST);
            if ($result[0] && $result_2[0]) {

                $data['resource'] = $result[1];
                $data['resource_2'] = $result_2[1]; 

                $this->about->create($data);
                $this->response->set_message(translate("message_save_form_ok"), ResponseMessage::SUCCESS);
                redirect("about/index", "location", 301);
            } else {

                if ($_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE) $result[1] = translate('message_file_size_exceeded_php_ini');
                if ($_FILES['archivo_2']['error'] == UPLOAD_ERR_INI_SIZE) $result_2[1] = translate('message_file_size_exceeded_php_ini');

                $this->response->set_message($result[1] . '<br>' . $result_2[1], ResponseMessage::ERROR);
                redirect("about/index", "location", 301);
            }

        } else {

            $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
            redirect("about/index/", "location", 301);
        }
    }



    public function update()
    {
        $this->validate_rol([1]);

        $text_first = $this->input->post('text-first');
        $text_mision = $this->input->post('text-mision');
        $text_vision = $this->input->post('text-vision');
        $text_second = $this->input->post('text-second');
        $number_count_1 = $this->input->post('number-count-1');
        $number_count_2 = $this->input->post('number-count-2');
        $number_count_3 = $this->input->post('number-count-3');
        $title_count_1 = $this->input->post('title-count-1');
        $title_count_2 = $this->input->post('title-count-2');
        $title_count_3 = $this->input->post('title-count-3');
        $about_id = $this->input->post('about-id');
        $old_file = $this->input->post('old-file');
        $old_file_2 = $this->input->post('old-file-2');


        // $this->form_validation->set_rules('title', translate('form_label_title_lang'), 'trim|required');
        // $this->form_validation->set_rules('phone', translate('form_label_phone_lang'), 'trim|required');
        // $this->form_validation->set_rules('address', translate('form_label_address_lang'), 'trim|required');
        // $this->form_validation->set_rules('latitude', translate('contact_label_latitude_lang'), 'trim|required');
        // $this->form_validation->set_rules('longuitude', translate('contact_label_longuitude_lang'), 'trim|required');
        // $this->form_validation->set_rules('email', translate('form_label_email_lang'), 'trim|required');

        // if ($this->form_validation->run() === FALSE) {
        //     $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
        //     redirect("about/index", 'location', 301);
        // }

        $data = [
            'text_first' => $text_first,
            'text_second' => $text_second,
            'text_vision' => $text_vision,
            'number_count_1' => $number_count_1,
            'number_count_2' => $number_count_2,
            'number_count_3' => $number_count_3,
            'title_count_1' => $title_count_1,
            'title_count_2' => $title_count_2,
            'title_count_3' => $title_count_3,
            'text_mision' => $text_mision,
        ];

        $update_file = FALSE;
        $update_file_2 = FALSE; 

        $name_file = $_FILES['archivo']['name'];
        $ofile_name = explode('/', $old_file);

        $name_file_2 = $_FILES['archivo-2']['name'];
        $ofile_name_2 = explode('/', $old_file_2);

        if (end($ofile_name) != $name_file and $name_file != null) {
            $update_file = TRUE;
        }

        if (end($ofile_name_2) != $name_file_2 and $name_file_2 != null) {
            $update_file_2 = TRUE;
        }

        $update_resource_1 = FALSE;
        $update_resource_2 = FALSE;

        if (!$update_file && !$update_file_2) {

            $r = $this->about->update($about_id, $data);

            if ($r === false) {

                $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                redirect("about/index/", "location", 301);
            } else {

                $this->response->set_message(translate("message_update_form_ok"), ResponseMessage::SUCCESS);
                redirect("about/index/", "location", 301);
            }
        } else {
            
            if($update_file){
                $separado = explode('.', $name_file);
                $ext = end($separado); // me quedo con la extension
                $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
                $allow_extension = in_array($ext, $allow_extension_array);

                if ($allow_extension) {

                    $result = save_image_from_post('archivo', './uploads/about', time(), $this::WIDTH, $this::HEIGHT);

                    if ($result[0]) {
                        if ($old_file != null) {
                            unlink($old_file);
                        }

                        $data['resource'] = $result[1];

                        $update_resource_1 = TRUE;
                        
                    } else {

                        if ($_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE) $result[1] = translate('message_file_size_exceeded_php_ini');

                        $this->response->set_message($result[1], ResponseMessage::ERROR);
                        // redirect("about/index/", "location", 301);
                    }

                } else {

                    $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
                    redirect("about/index/", "location", 301);
                }

            }

            if($update_file_2){
                
                $separado = explode('.', $name_file_2);
                $ext = end($separado); // me quedo con la extension
                $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
                $allow_extension = in_array($ext, $allow_extension_array);

                if ($allow_extension) {

                    $result_2 = save_image_from_post('archivo-2', './uploads/about', time(), $this::WIDTH_IMG_FIRST, $this::HEIGHT_IMG_FIRST);

                    if ($result_2[0]) {
                        if ($old_file_2 != null) {
                            unlink($old_file_2);
                        }

                        $data['resource_2'] = $result_2[1];

                        $update_resource_2 = TRUE;
                        
                    } else {

                        if ($_FILES['archivo-2']['error'] == UPLOAD_ERR_INI_SIZE) $result_2[1] = translate('message_file_size_exceeded_php_ini');

                        $this->response->set_message($result_2[1], ResponseMessage::ERROR);
                        // redirect("about/index/", "location", 301);
                    }

                } else {

                    $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
                    redirect("about/index/", "location", 301);
                }

            }
            
            if ($update_resource_1 || $update_resource_2 ) {
                
                $this->about->update($about_id, $data);
                
                $this->response->set_message(translate("message_update_form_ok"), ResponseMessage::SUCCESS);
                // redirect("about/index/", "location", 301);
            } else {
                $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                // redirect("about/index/", "location", 301);
            }

            redirect("about/index/", "location", 301);
        }
    }

}
