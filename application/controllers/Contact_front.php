<?php
defined('BASEPATH') or exit('No direct script access allowed');
//TODO: Agregar adminstrable el texto descriptivo de la pagina de contacto
class Contact_front extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Contact_model', 'contact');

		@session_start();
	}


	public function index()
	{
		$contact = $this->contact->get();
		
		if(!$contact){
			
			$contact = (object)[
				"title" => 'Déjenos un Mensaje',
				"phone" => 'PBX (593-2) 2903336 - Telefax: 3237720/8002',
				"email" => 'info@promostock.com.ec',
				"address" => 'Av. Orellana.E12-126 y 12 de Octubre, Quito, Ecuador',
				"resource" => './assets/images/slider-example/slide-1.jpg',
				"location" => (object)[
					'latitude' => -0.1862504,
					'longuitude' => -78.5706263,
					'latitude_2' => -2.1397483,
					'longitude_2' => -79.8694064
				]
			];
		}

		$phones = explode(',',$contact->phone);
		foreach($phones as $i => $phone){
			$phones[$i] = [
				'tel' =>  str_replace(')', '', str_replace('(', '',trim($phone))),
				'string' => str_replace('+','', trim($phone))
			];
		}
		$contact->phone = $phones;
		$data['contact'] = $contact;
		$this->load_view_front('contact', $data);
	}
}
