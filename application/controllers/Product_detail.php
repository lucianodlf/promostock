<?php

class Product_detail extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Product_model', 'product');

        @session_start();
        $this->init_form_validation();
    }


    public function index($id = 0)
    {
        $product = $this->product->get_by_id($id);

        if (!$product) {
            $this->response->set_message('El producto a visualizar no existe', ResponseMessage::ERROR);
            redirect("home", "location", 301);
        }

        $this->product->update($id,['views' => (int) $product->views + 1]);

        $proudcts_related = $this->product->get_related(10, $product->category, $product->code);
        $product_review = $this->product->get_all_reviews_by_product_id($id);

        $data['product_reviews'] = $product_review;
        $data['product'] = $product;
        $data['products_related'] = $proudcts_related;

        $this->load_view_front("product_detail", $data);
    }



    public function save_review()
    {
        //$this->validate_rol([1,2]);

        $product_id = $this->input->post('product_id');
        $user_name = $this->input->post('user_name');
        $user_email = $this->input->post('user_email');
        $user_id = $this->session->userdata('user_id');
        $text_review = $this->input->post('text_review');

        $this->form_validation->set_rules('user_name', translate('ph_review_user_name'), 'trim|required');
        $this->form_validation->set_rules('user_email', translate('ph_review_user_email'), 'trim|required');
        $this->form_validation->set_rules('text_review', translate('ph_review_text'), 'trim|required');

        if ($this->form_validation->run() == FALSE) {

            // $return = [
            //     'status' => FALSE,
            //     'msg' => validation_errors()
            // ];
            // echo json_encode($return);
            // return 0;

            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            redirect("product_detail/" . $product_id, "location", 301);
        }

        if (!$this->session->userdata('user_id')) {
            $this->load->model('User_model', 'user');
            $user_exist = $this->user->get_by_email($user_email);
            $user_id = NULL;
            $user_photo = 'assets/avatars/juice.png';

            if ($user_exist) {
                $user_id = $user_exist->_id;
                $user_name = $user_exist->name;
                if ($user_exist->photo) {
                    $user_photo = $user_exist->photo;
                }
            }
        } else {
            $user_id = $this->session->userdata('user_id');
            $user_name = $this->session->userdata('name');
            $user_photo = $this->session->userdata('photo');
        }


        $data_save_db = [
            'product' => (string) $product_id,
            'user_id' => (string) $user_id,
            'user_name' => (string) $user_name,
            'user_email' => (string) $user_email,
            'user_photo' => (string) $user_photo,
            'text_review' => (string) $text_review,
            'created_at' => time()
        ];

        $new_id = $this->product->create_review($data_save_db);

        if (!$new_id) {
            // $return = [
            //     'status' => FALSE,
            //     'msg' => translate('message_error_create_review')
            // ];

            // echo json_encode($return);
            // return 0;

            $this->response->set_message(translate('message_error_create_review'), ResponseMessage::ERROR);
            redirect("product_detail/" . $product_id, "location", 301);
        }


        // $return = [
        //     'status' => TRUE,            
        // ];

        // echo json_encode($return);
        // return 0;

        $this->response->set_message(translate('message_save_review_ok'), ResponseMessage::SUCCESS);
        redirect("product_detail/" . $product_id, "location", 301);
    }
}
