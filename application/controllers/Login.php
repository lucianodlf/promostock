<?php

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('User_model', 'user');

        @session_start();
        $this->init_form_validation();
    }


    public function index()
    {
        $this->load->view("login");
    }



    public function auth()
    {

        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));

        $user = $this->user->get_user_auth($email, $password);

        if ($user) {
            $session_data = object_to_array($user);

            // Change key value of user id for compatibility
            if (isset($session_data['_id'])) {
                $session_data['user_id'] = $session_data['_id'];
                unset($session_data['_id']);
            }

            $this->session->set_userdata($session_data);

            if ($user->role_id == 1) {

                // redirect("dashboard/index");

                $resp = [
                    'msg' => translate('autenticacion_error_lang'),
                    'url' => site_url('dashboard/index'),
                    'is_valid' => TRUE
                ];

                echo json_encode($resp);

            } else {

                // redirect(site_url());
                
                $resp = [
                    'msg' => translate('autenticacion_error_lang'),
                    'url' => site_url(),
                    'is_valid' => TRUE
                ];

                echo json_encode($resp);
            }
        } else {

            // $this->response->set_message(translate('autenticacion_error_lang'), ResponseMessage::ERROR);
            // redirect("home/#popup-login");
            $resp = [
                'msg' => translate('autenticacion_error_lang'),
                'is_valid' => FALSE
            ];

            echo json_encode($resp);
        }
    }



    public function logout()
    {
        parent::log_out();
        redirect(site_url());
    }



    public function recover_password_index()
    {
        $this->load->view("recover_password");
    }



    public function recover_password()
    {
        $email = $this->input->post("email");

        $this->load->model("User_model", "user");

        $user_object = $this->user->get_by_email($email);

        if ($user_object) {

            $new_password = time();
            $this->user->update($user_object->_id, ["password" => md5($new_password)]);

            $this->load->library('email');

            $this->load->config('email');

            $this->email->initialize();

            $this->email->from($this->email->smtp_user, 'PromoStock');

            $this->email->to($email);

            $this->email->subject(translate('msg_subject_recover_pwd'));
            $mensaje = "Estimado usuario: <br/> La contraseña ha sido generada satisfactoriamente.  <br/> Su nueva contraseña es: <b>" . $new_password . ".  <br> 
            <br> 
            Muchas gracias";

            $this->email->message($mensaje);

            $this->email->send();

            $this->response->set_message(translate('message_change_email_lang'), ResponseMessage::SUCCESS);
            redirect("login");
        } else {

            $this->response->set_message(translate('message_user_not_exist_lang'), ResponseMessage::ERROR);
            redirect("login");
        }
    }
}
