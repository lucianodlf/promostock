<?php

class Footer extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Footer_model', 'footer');

        @session_start();
        $this->init_form_validation();
    }



    public function index()
    {

        $this->validate_rol([1]);

        $footer_object = $this->footer->get();

        if (!$footer_object) {
            $footer_object = (object) [
                '_id' => null,
                'title_1' => '',
                'title_2' => '',
                'title_3' => '',
                'title_4' => '',
                'link_fb' => '',
                'link_instagram' => '',
                'link_linkedin' => '',
                'link_youtube' => '',
                'link_twitter' => '',
                'text' => ''
            ];
        }
        $footer_object->link_linkedin = '';

        $data['footer_object'] = $footer_object;
        $this->load_view_admin_g("footer/index", $data);
    }



    public function add()
    {
        $this->validate_rol([1]);

        $title_1 = $this->input->post('title-1');
        $title_2 = $this->input->post('title-2');
        $title_3 = $this->input->post('title-3');
        $title_4 = $this->input->post('title-4');
        $text = $this->input->post('text');
        $link_fb = $this->input->post('link-fb');
        $link_instagram = $this->input->post('link-instagram');
        $link_linkedin = $this->input->post('link-linkedin');

        $this->form_validation->set_rules('title-1', translate('footer_title_1_lang'), 'trim|required');
        $this->form_validation->set_rules('title-2', translate('footer_title_2_lang'), 'trim|required');
        $this->form_validation->set_rules('title-3', translate('footer_title_3_lang'), 'trim|required');
        $this->form_validation->set_rules('title-4', translate('footer_title_4_lang'), 'trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            redirect("footer/index", 'location', 301);
        }

        $data = [
            'title_1' => $title_1,
            'title_2' => $title_2,
            'title_3' => $title_3,
            'title_4' => $title_4,
            'link_fb' => $link_fb,
            'link_instagram' => $link_instagram,
            'link_linkedin' => $link_linkedin,
            'link_youtube' => '',
            'link_twitter' => '',
            'text' => $text
        ];

        $this->footer->create($data);
        $this->response->set_message(translate("message_save_form_ok"), ResponseMessage::SUCCESS);
        redirect("footer/index", "location", 301);

    }



    public function update()
    {
        $this->validate_rol([1]);

        $title_1 = $this->input->post('title-1');
        $title_2 = $this->input->post('title-2');
        $title_3 = $this->input->post('title-3');
        $title_4 = $this->input->post('title-4');
        $text = $this->input->post('text');
        $link_fb = $this->input->post('link-fb');
        $link_instagram = $this->input->post('link-instagram');
        $link_linkedin = $this->input->post('link-linkedin');
        $footer_id = $this->input->post('footer-id');

        $this->form_validation->set_rules('title-1', translate('footer_title_1_lang'), 'trim|required');
        $this->form_validation->set_rules('title-2', translate('footer_title_2_lang'), 'trim|required');
        $this->form_validation->set_rules('title-3', translate('footer_title_3_lang'), 'trim|required');
        $this->form_validation->set_rules('title-4', translate('footer_title_4_lang'), 'trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->response->set_message(validation_errors(), ResponseMessage::ERROR);
            redirect("footer/index", 'location', 301);
        }

        $data = [
            'title_1' => $title_1,
            'title_2' => $title_2,
            'title_3' => $title_3,
            'title_4' => $title_4,
            'link_fb' => $link_fb,
            'link_instagram' => $link_instagram,
            'link_linkedin' => $link_linkedin,
            'link_youtube' => '',
            'link_twitter' => '',
            'text' => $text
        ];

        $this->footer->update($footer_id, $data);
        $this->response->set_message(translate("message_save_form_ok"), ResponseMessage::SUCCESS);
        redirect("footer/index", "location", 301);
    }
}
