<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Banner_model', 'banner');
		$this->load->model('OurClient_model', 'ourcli');
		$this->load->model('Product_model', 'product');

		@session_start();
	}


	public function index()
	{

		// Get banners
		$banners = $this->banner->get_all_active();

		if ($banners) {
			foreach ($banners as $banner) {
				$banner->btn_link_blank = FALSE;
				preg_match('@^(?:http://)?([^/]+)@i', $banner->btn_link, $matches);
				if (isset($matches[1])) {
					if ($matches[1] !== $_SERVER['HTTP_HOST']) {
						$banner->btn_link_blank = TRUE;
					}
				}
			}
		}

		// Get Our Clients
		$oru_clients = $this->ourcli->get_all_ourclient_active();

		// Get Products news
		$products_new = $this->product->get_news(10);

		// Get Products deal
		$products_deal = $this->product->get_deals(10);

		// Get Products popular
		$products_popular = $this->product->get_most_populars(10);

		// Get Products most views
		$products_most_views = $this->product->get_most_views(10);

		// Get Products show deal banner
		$products_deal_banner = $this->product->get_deal_banner(10);

		$data['products_new'] = $products_new;
		$data['products_popular'] = $products_popular;
		$data['products_most_views'] = $products_most_views;
		$data['products_deal'] = $products_deal;
		$data['products_deal_banner'] = $products_deal_banner;
		$data['banners'] = $banners;
		$data['our_clients'] = $oru_clients;

		$this->load_view_front('home', $data);
	}

	public function suscriber(){
		//TODO: Datos de stmp para envio, mensaje de suscripcion. ¿gestionables?
		//TODO: Metodo para cancelar subscripcion?
		//TODO: Guardar en base de datos
		
		$email_to = $this->input->post('email');

		$this->form_validation->set_rules('email', translate('contact_label_email_lang'), 'trim|required');
		
		if ($this->form_validation->run() === FALSE) {
			$this->response->set_message(validation_errors(), ResponseMessage::ERROR);
			redirect(site_url(), 'location', 301);
		}

		$this->load->model('Contact_model', 'contact');
		
		$new_contact_db = [
			'name' => '',
			'email' => (string) $email_to,
			'phone' => '',
			'subject' => '',
			'message' => '',
			'for_subcriber' => (int) 1,
			'create_at' => time()
		];
        
		$this->contact->add_new_contact($new_contact_db);

		$this->load->library('email');

		$this->load->config('email');

		$this->email->initialize();

		$this->email->from($this->email->smtp_user, 'PromoStock');

		$this->email->to($email_to);

		$this->email->subject("Suscripcion a noticias - PromoStock");
		$mensaje = "Muchas gracias por suscribirse a nuestras noticias!";

		$this->email->message($mensaje);

		$this->email->send();

		$this->response->set_message('Suscribcion realizada con exito!', ResponseMessage::SUCCESS);
		redirect("home");


	}
}
