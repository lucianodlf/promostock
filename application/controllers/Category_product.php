<?php

class Category_product extends CI_Controller
{

    private const WIDTH = 410;
    private const HEIGHT = 220;
    private $data;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Category_product_model', 'category');

        $this->data['img_width'] = $this::WIDTH;
        $this->data['img_height'] = $this::HEIGHT;

        @session_start();
        $this->init_form_validation();

    }


    public function index()
    {

        $this->validate_rol([1]);

        $all_category_prod = $this->category->get_all();
        $this->data['all_category_prod'] = $all_category_prod;
        $this->load_view_admin_g("category_product/index", $this->data);
    }



    public function add_index()
    {
        $this->validate_rol([1]);

        $this->load_view_admin_g('category_product/add', $this->data);
    }



    public function add()
    {
        $this->validate_rol([1]);

        $name = $this->input->post('name');

        $this->data = [
            'name' => $name,
            'resource' => null,
            'is_active' => 1
        ];

        $name_file = $_FILES['archivo']['name'];
        $separado = explode('.', $name_file);
        $ext = end($separado); // me quedo con la extension
        $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
        $allow_extension = in_array($ext, $allow_extension_array);

        if ($allow_extension) {

            $result = save_image_from_post('archivo', './uploads/category_product', time(), $this::WIDTH, $this::HEIGHT);
            if ($result[0]) {

                $this->data['resource'] = $result[1];

                $this->category->create($this->data);
                $this->response->set_message(translate("message_save_category_prod_ok"), ResponseMessage::SUCCESS);
                redirect("category_product/index", "location", 301);
            } else {

                if( $_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE ) $result[1] = translate('message_file_size_exceeded_php_ini');
                
                $this->response->set_message($result[1], ResponseMessage::ERROR);
                redirect("category_product/add_index", "location", 301);
            }
        } else {

            $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
            redirect("category_product/add_index/", "location", 301);
        }
    }


    function update_index($category_id = 0)
    {
        $this->validate_rol([1]);

        $category_prod_object = $this->category->get_by_id($category_id);

        if ($category_prod_object) {

            $this->data['category_prod_object'] = $category_prod_object;
            $this->load_view_admin_g('category_product/update', $this->data);
        } else {

            show_404();
        }
    }



    public function update_status($status, $id)
    {
        $this->validate_rol([1]);

        $this->data = [
            'is_active' => $status,
        ];
        
        $r = $this->category->update($id, $this->data);
        if ($r === false) {

            $this->response->set_message(translate("message_error"), ResponseMessage::SUCCESS);
            redirect("category_product/index/", "location", 301);
        } else {

            $this->response->set_message(translate("message_update_category_prod_ok"), ResponseMessage::SUCCESS);
            redirect("category_product/index/", "location", 301);
        }
    }



    public function update()
    {
        $this->validate_rol([1]);

        $name = $this->input->post('name');
        $category_id = $this->input->post('category_id');
        $old_file = $this->input->post('old-file');

        $this->data = [
            'name' => $name,
            'is_active' => 0
        ];

        $update_file = FALSE;

        $name_file = $_FILES['archivo']['name'];
        $ofile_name = explode('/', $old_file);

        if (end($ofile_name) != $name_file and $name_file != null) {
            $update_file = TRUE;
        }

        if (!$update_file) {

            $r = $this->category->update($category_id, $this->data);

            if ($r === false) {

                $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                redirect("category_product/index/", "location", 301);
            } else {

                $this->response->set_message(translate("message_update_category_prod_ok"), ResponseMessage::SUCCESS);
                redirect("category_product/index/", "location", 301);
            }
        } else {

            $separado = explode('.', $name_file);
            $ext = end($separado); // me quedo con la extension
            $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
            $allow_extension = in_array($ext, $allow_extension_array);

            if ($allow_extension) {

                $result = save_image_from_post('archivo', './uploads/category_product', time(), $this::WIDTH, $this::HEIGHT);
                if ($result[0]) {
                    if ($old_file != null) {
                        unlink($old_file);
                    }

                    $this->data['resource'] = $result[1];

                    $r = $this->category->update($category_id, $this->data);
                    if ($r > 0) {
                        $this->response->set_message(translate("message_save_category_prod_ok"), ResponseMessage::SUCCESS);
                        redirect("category_product/index/", "location", 301);
                    } else {
                        $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                        redirect("category_product/index/", "location", 301);
                    }
                } else {

                    if( $_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE ) $result[1] = translate('message_file_size_exceeded_php_ini');

                    $this->response->set_message($result[1], ResponseMessage::ERROR);
                    redirect("category_product/index/", "location", 301);
                }
            } else {

                $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
                redirect("category_product/index/", "location", 301);
            }
        }
    }



    public function delete($category_id = 0)
    {
        $this->validate_rol([1]);

        $category_prod_object = $this->category->get_by_id($category_id);

        if ($category_prod_object) {

            if (file_exists($category_prod_object->resource)) {
                unlink($category_prod_object->resource);
            }

            $this->category->delete($category_id);

            $this->response->set_message(translate('message_delete_category_prod_ok'), ResponseMessage::SUCCESS);
            redirect("category_product/index", "location", 301);
        } else {

            $this->response->set_message(translate('message_error'), ResponseMessage::SUCCESS);
            redirect("category_product/index", "location", 301);
        }
    }
}
