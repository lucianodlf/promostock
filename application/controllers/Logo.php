<?php

class Logo extends CI_Controller
{

    private const WIDTH = 170;
    private const HEIGHT = 57;
    private $data;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Logo_model', 'logo');

        $this->data = [
            'img_width' => $this::WIDTH,
            'img_height' => $this::HEIGHT
        ];

        @session_start();
        $this->init_form_validation();
    }


    public function index()
    {

        $this->validate_rol([1]);

        $all_logos = $this->logo->get_all();
        $this->data['all_logos'] = $all_logos;
        $this->load_view_admin_g("logo/index", $this->data);
    }



    public function add_index()
    {
        $this->validate_rol([1]);

        $this->load_view_admin_g('logo/add', $this->data);
    }



    public function add()
    {
        $this->validate_rol([1]);

        $text = $this->input->post('text');

        /**
         * section = ['header', 'shrinking-navbar', 'footer']
         */
        $section = $this->input->post('section');

        if (is_null($section)) {
            $section = 'header';
        }

        $data = [
            'text' => $text,
            'resource' => null,
            'is_active' => 0,
            'section' => $section
        ];

        $name_file = $_FILES['archivo']['name'];
        $separado = explode('.', $name_file);
        $ext = end($separado); // me quedo con la extension
        $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
        $allow_extension = in_array($ext, $allow_extension_array);

        if ($allow_extension) {

            $result = save_image_from_post('archivo', './uploads/logo', time(), $this::WIDTH, $this::HEIGHT);
            if ($result[0]) {

                $data['resource'] = $result[1];

                $this->logo->create($data);
                $this->response->set_message(translate("message_save_logo_ok"), ResponseMessage::SUCCESS);
                redirect("logo/index", "location", 301);
            } else {

                if ($_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE) $result[1] = translate('message_file_size_exceeded_php_ini');

                $this->response->set_message($result[1], ResponseMessage::ERROR);
                redirect("logo/add_index", "location", 301);
            }
        } else {

            $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
            redirect("logo/add_index/", "location", 301);
        }
    }


    function update_index($logo_id = 0)
    {
        $this->validate_rol([1]);

        $logo_object = $this->logo->get_by_id($logo_id);

        if ($logo_object) {

            $this->data['logo_object'] = $logo_object;
            $this->load_view_admin_g('logo/update', $this->data);
        } else {

            show_404();
        }
    }



    public function update_status($status, $id)
    {
        $this->validate_rol([1]);

        $data = [
            'is_active' => intval($status),
        ];

        if ($status == 1) {

            $active_logos = $this->logo->get_actives();

            if ($active_logos) {

                if (count((array) $active_logos) == 3) {
                    $this->response->set_message(translate("message_max_limit_active_logos"), ResponseMessage::ERROR);
                    redirect("logo/index", "location", 301);
                }

                $current_logo = $this->logo->get_by_id($id);

                if (!$this->__validate_one_logo_for_section($current_logo->section)) {
                    $this->response->set_message(translate("message_limit_logo_for_section"), ResponseMessage::ERROR);
                    redirect("logo/index", "location", 301);
                }
            }
        }

        //if($status == 1) $this->logo->disable_all();

        $r = $this->logo->update($id, $data);
        if ($r === false) {

            $this->response->set_message(translate("message_error"), ResponseMessage::SUCCESS);
            redirect("logo/index/", "location", 301);
        } else {

            $this->response->set_message(translate("message_update_logo_ok"), ResponseMessage::SUCCESS);
            redirect("logo/index/", "location", 301);
        }
    }



    public function update()
    {
        $this->validate_rol([1]);

        $text = $this->input->post('text');
        $logo_id = $this->input->post('logo_id');
        $old_file = $this->input->post('old-file');

        /**
         * section = ['header', 'shrinking-navbar', 'footer']
         */
        $section = $this->input->post('section');

        if (is_null($section)) {
            $section = 'header';
        }

        $current_logo = $this->logo->get_by_id($logo_id);

        if ($current_logo->is_active == 1) {

            if ($current_logo->section != $section) {

                if (!$this->__validate_one_logo_for_section($section)) {
                    $this->response->set_message(translate("message_limit_logo_for_section"), ResponseMessage::ERROR);
                    redirect("logo/index", "location", 301);
                }
            }
        }

        $data = [
            'text' => $text,
            'section' => $section
        ];

        $update_file = FALSE;

        $name_file = $_FILES['archivo']['name'];
        $ofile_name = explode('/', $old_file);

        if (end($ofile_name) != $name_file and $name_file != null) {
            $update_file = TRUE;
        }

        if (!$update_file) {

            $r = $this->logo->update($logo_id, $data);

            if ($r === false) {

                $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                redirect("logo/index/", "location", 301);
            } else {

                $this->response->set_message(translate("message_update_logo_ok"), ResponseMessage::SUCCESS);
                redirect("logo/index/", "location", 301);
            }
        } else {

            $separado = explode('.', $name_file);
            $ext = end($separado); // me quedo con la extension
            $allow_extension_array = ["JPEG", "JPG", "jpg", "jpeg", "png", "bmp", "gif"];
            $allow_extension = in_array($ext, $allow_extension_array);

            if ($allow_extension) {

                $result = save_image_from_post('archivo', './uploads/logo', time(), $this::WIDTH, $this::HEIGHT);
                if ($result[0]) {
                    if ($old_file != null) {
                        unlink($old_file);
                    }

                    $data['resource'] = $result[1];

                    $r = $this->logo->update($logo_id, $data);
                    if ($r > 0) {
                        $this->response->set_message(translate("message_save_logo_ok"), ResponseMessage::SUCCESS);
                        redirect("logo/index/", "location", 301);
                    } else {
                        $this->response->set_message(translate("message_error"), ResponseMessage::ERROR);
                        redirect("logo/index/", "location", 301);
                    }
                } else {

                    if ($_FILES['archivo']['error'] == UPLOAD_ERR_INI_SIZE) $result[1] = translate('message_file_size_exceeded_php_ini');

                    $this->response->set_message($result[1], ResponseMessage::ERROR);
                    redirect("logo/index/", "location", 301);
                }
            } else {

                $this->response->set_message(translate("message_file_tipe_not_accept"), ResponseMessage::ERROR);
                redirect("logo/index/", "location", 301);
            }
        }
    }



    public function delete($logo_id = 0)
    {
        $this->validate_rol([1]);

        $logo_object = $this->logo->get_by_id($logo_id);

        if ($logo_object) {

            if (file_exists($logo_object->resource)) {
                unlink($logo_object->resource);
            }

            $this->logo->delete($logo_id);

            $this->response->set_message(translate('message_delete_logo_ok'), ResponseMessage::SUCCESS);
            redirect("logo/index", "location", 301);
        } else {

            $this->response->set_message(translate('message_error'), ResponseMessage::SUCCESS);
            redirect("logo/index", "location", 301);
        }
    }

    private function __validate_one_logo_for_section($section)
    {

        $logos_of_section = $this->logo->get_by_section($section);

        if (!$logos_of_section) {
            return TRUE;
        }
        return FALSE;
    }
}
