$(function () {
	$("#alert-error-msg").hide();

	$('#form-login').find('input[name=password]').keyup(function (e) {
		if (e.keyCode == 13) {
			login();
		}
	});

});

$(window).on('load', function () {
	// let url = window.location.href;
	// let url_splited = url.split('#');

	// if (url_splited[url_splited.length - 1] === 'popup-login') {
	// 	$.magnificPopup.open({
	// 		items: {
	// 			src: '#popup-login',
	// 			type: 'inline'
	// 		}
	// 	}, 0);
	// }
	// console.debug('load page');

	setEventClickNavBarItem();
});


$(document).ready(function () {
	// console.debug('ready documment');
	// $(".alert").fadeIn(500);

	setTimeout(function () {
		$(".alert").fadeOut(500);
	}, 4000);

});

function login() {
	$("#alert-error-msg").hide();
	let form_login = $('#form-login');

	let email = form_login.find('input[name=email]').val();
	let pwd = form_login.find('input[name=password]').val();

	if (email == '' || pwd == '') {
		return 0;
	}

	$.post('login/auth', {
		email: form_login.find('input[name=email]').val(),
		pwd: form_login.find('input[name=password]').val()

	}).done((resp) => {

		resp = JSON.parse(resp);
		console.log(resp);

		if (resp.is_valid === false) {
			console.log('mostar mensaje');
			$("#alert-error-msg").html(resp.msg);
			$("#alert-error-msg").show();
			form_login.find('input[name=password]').focus();

			setTimeout(function () {
				$(".alert").fadeOut(500);
			}, 4000);
		} else {
			window.location.href = resp.url;

		}
	});
}


function setEventClickNavBarItem() {
	$("li.nav-item").click(function () {
		$("li.nav-item").removeClass('active');
		$(this).addClass('active');
	});
}


function showPopupProductDetail(id, callback) {

	//console.log(site_url);

	$.post(site_url + '/product/get_detail', {
		id: id
	}).done((resp) => {

		resp = JSON.parse(resp);
		// console.log(resp);

		if (resp.status === false) {
			console.log(resp.msg);

			$("#alert-error-msg").html(resp.msg);
			$("#alert-error-msg").show();

			// setTimeout(function () {
			// 	$(".alert").fadeOut(500);
			// }, 4000);

			return false;

		} else {

			$("#popup-detail-product .quickview-product-detail .box-title").html(resp.name);
			$("#popup-detail-product .quickview-product-detail .box-price").html(resp.price_final);
			$("#popup-detail-product .quickview-product-detail .box-text").html(resp.description);

			$("#popup-detail-product .product-image .product_img").attr('data-zoom-image', site_url + resp.resource_main);
			$("#popup-detail-product .product-image .product_img").attr('src', site_url + resp.resource_main);

			$("#popup-detail-product .product_gallery_item .item .active").attr('data-image', site_url + resp.resource_main);
			$("#popup-detail-product .product_gallery_item .item .active").attr('data-zoom-image', site_url + resp.resource_main);
			$("#popup-detail-product .product_gallery_item .item .active img").attr('src', site_url + resp.resource_small);

			if(resp.stock_quantity > 0){
				$("#popup-detail-product .quickview-product-detail .stock").html('Disponibilidad: <span>Disponible</span>');
			}else{
				$("#popup-detail-product .quickview-product-detail .stock").html('Disponibilidad: <span>No disponible por el momento</span>');
			}
			
			callback(true);
		}
	});
}


function sendProductReview(product_id){
	console.log(product_id);

	let form = $("#form-product-review");
	
	let user_name = form.find("input[name='user_name']").val();
	let user_email = form.find("input[name='user_email']").val();
	let text_review = form.find("textarea[name='text_review']").val();

	console.log(user_name);
	console.log(user_email);
	console.log(text_review);

	return false;

	$.post(site_url + '/product_detail/save_review', {
		product_id: product_id,
	}).done((resp) => {

		resp = JSON.parse(resp);
		// console.log(resp);

		if (resp.status === false) {
			console.log(resp.msg);

			$("#alert-error-msg").html(resp.msg);
			$("#alert-error-msg").show();

			// setTimeout(function () {
			// 	$(".alert").fadeOut(500);
			// }, 4000);

			return false;

		} else {

			$("#popup-detail-product .quickview-product-detail .box-title").html(resp.name);
			$("#popup-detail-product .quickview-product-detail .box-price").html(resp.price_final);
			$("#popup-detail-product .quickview-product-detail .box-text").html(resp.description);

			$("#popup-detail-product .product-image .product_img").attr('data-zoom-image', site_url + resp.resource_main);
			$("#popup-detail-product .product-image .product_img").attr('src', site_url + resp.resource_main);

			$("#popup-detail-product .product_gallery_item .item .active").attr('data-image', site_url + resp.resource_main);
			$("#popup-detail-product .product_gallery_item .item .active").attr('data-zoom-image', site_url + resp.resource_main);
			$("#popup-detail-product .product_gallery_item .item .active img").attr('src', site_url + resp.resource_small);

			if(resp.stock_quantity > 0){
				$("#popup-detail-product .quickview-product-detail .stock").html('Disponibilidad: <span>Disponible</span>');
			}else{
				$("#popup-detail-product .quickview-product-detail .stock").html('Disponibilidad: <span>No disponible por el momento</span>');
			}
			
			callback(true);
		}
	})
}
